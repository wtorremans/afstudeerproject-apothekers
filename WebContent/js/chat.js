var xHRObject = new XMLHttpRequest();

function removeAllChilds(parent) {
	if (parent != null) {
		var range = new Range();
		range.selectNodeContents(parent);
		range.deleteContents();
	}
}

function onload() {
	getMessages();
}

function getMessages() {
	xHRObject.open("GET", "Controller?action=GetMessages&friend=" + document.getElementById("matchApb").innerHTML, true);
	xHRObject.onreadystatechange = Messages;
	xHRObject.send(null);
}

function Messages(serverResponse) {
	if (xHRObject.readyState == 4) {
		if (xHRObject.status == 200) {
			var serverResponse = JSON.parse(xHRObject.responseText);
			var messages = serverResponse["messages"];
			var list = document.getElementById("messages");
			var user = document.getElementById("invisible").innerHTML;
			removeAllChilds(list);
			
			for (var i=0; i < messages.length; i++) {
				var message = document.createElement("div");
				var sender = document.createElement("p");
				
				var text = document.createElement("p");
				
				var time = document.createElement("p");
				
				
				var message_sender = document.createTextNode(messages[i]["sender"]["naam"]);
				var senderApb = messages[i]["sender"]["apb"];
				var receiverApb = messages[i]["receiver"]["apb"];
				var message_time = document.createTextNode(messages[i]["time"]);
				var message_text = document.createTextNode(messages[i]["message"]);
				if (user == senderApb)
					{
						sender.setAttribute('class', 'messageSender');
						text.setAttribute('class', 'messageBoxSender');
						time.setAttribute('class', 'messageTimeSender');
					}
				else {
					sender.setAttribute('class', 'messageReceiver');
					text.setAttribute('class', 'messageBoxReceiver');
					time.setAttribute('class', 'messageTimeReceiver');
				}
				sender.appendChild(message_sender);
				text.appendChild(message_text);
				time.appendChild(message_time);
				
				message.appendChild(sender);
				message.appendChild(text);
				message.appendChild(time);
				
				list.appendChild(message);
				var myDiv = document.getElementById("messages");
				myDiv.scrollTop = myDiv.scrollHeight;
			}
			setTimeout("getMessages()", 5000);
		}
	}
}

function sendMessage(id, receiver) {
	var input = document.getElementById("message");
	var message = input.value;
	input.value = "";
	
	$.ajax({
		type: 'POST',
		url: 'Controller?action=sendMessage&chatId=' + id + '&receiver=' + receiver +'&message=' + message,
		success: function() {
			getMessages();
		}
	})
}
