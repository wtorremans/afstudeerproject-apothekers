<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page="head.jsp">
	<jsp:param name="title" value="MedMatcher - Chat"/>
	<jsp:param name="chat" value="actual"/>
</jsp:include>

<c:choose>	
	<c:when test="${apotheek == null}">
		<div id="base">
        	<h1>Login</h1>
        	<c:forEach var="error" items="${errors }">
            	<p class="alert-danger">${error }</p>
        	</c:forEach>
        	<form method="POST" action="Controller?action=login">
            	<table>
					<tr>
						<td><label for="apb">APB-nummer:</label></td>
						<td><input type="text" id="apb" name="apb" placeholder="APB-nummer" autofocus/></td>
					</tr>
					<tr>
						<td><label for="paswoord">Paswoord:</label></td>
						<td><input type="password" id="paswoord" name="paswoord" placeholder="Paswoord"/></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" id="LogIn" value="Log In" autofocus/></td>
					</tr>
				</table>
        	</form>
       		<a href="Controller?action=registrationPage" id="registrationbutton"><button>Registreer</button></a>
        </div>
	</c:when>
	<c:otherwise>
	<p id="invisible">${apotheek.apb }</p>
		<div id="matches">
			<c:forEach var="chat" items="${ chats }">
				<c:choose>
					<c:when test="${ apotheek.apb == chat.a1.apb }">
						<div id="match" onclick="window.location='Controller?action=getChat&apotheker=${ chat.a2.apb }'">
							<h4><img class="profilePicture" src="uploadedImages/${chat.a2.fileName }"></img><br/><span id="userChat">${ chat.a2.naam } (<strong id="user">${ chat.a2.apb }</strong>)</span> </h4>
							<a href="Controller?action=deleteChat&id=${ chat.id }">Delete</a>
						</div>
					</c:when>
					<c:otherwise>
						<div id="match" onclick="window.location='Controller?action=getChat&apotheker=${ chat.a1.apb }'">
							<h4><img class="profilePicture" src="uploadedImages/${chat.a1.fileName }"></img><br/><span id="userChat">${ chat.a1.naam } (<strong id="user">${ chat.a1.apb }</strong>)</span></h4>
							<a href="Controller?action=deleteChat&id=${ chat.id }">Delete</a>
						</div>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</div>
		<div id="chatScreen">
		<c:choose>
			<c:when test="${ chat == null }">
				<h3>Selecteer match</h3>
			</c:when>
			<c:otherwise>
				<c:choose>
					<c:when test="${ apotheek.apb == chat.a1.apb }">
						<h3 id="userID"><img class="profilePicture" src="uploadedImages/${chat.a2.fileName }"></img><br/><span id="userChat">${ chat.a2.naam } (<strong id="matchApb">${ chat.a2.apb }</strong>)</span></h3>
						<div id="messages">
							
						</div>
						<div id="inputfield">
							<p><textarea name="message" id="message" placeholder="Bericht hier" autofocus/></textarea><button id="submit" onclick="sendMessage(${chat.id}, ${chat.a2.apb })">Verzend</button></p>
						</div>
					</c:when>
					<c:otherwise>
						<h3 id="userID"><img class="profilePicture" src="uploadedImages/${chat.a1.fileName }"></img><br/><span id="userChat">${ chat.a1.naam } (<strong id="matchApb">${ chat.a1.apb }</strong>)</span></h3>
						<div id="messages">
							
						</div>
						<div id="inputfield">
							<p><textarea name="message" id="message" autofocus/></textarea><button id="submit" onclick="sendMessage(${chat.id}, ${chat.a2.apb })">Verzend</button></p>
						</div>
					</c:otherwise>
				</c:choose>
			</c:otherwise>
		</c:choose>	
		</div>
	</c:otherwise>
</c:choose>
<script type="text/javascript" src="js/chat.js"></script>
<script>
$("#messages").scrollTop($("#messages")[0].scrollHeight);
</script>
</body>
</html>