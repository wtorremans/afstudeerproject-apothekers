<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<jsp:include page="head.jsp">
	<jsp:param name="title" value="MedMatcher - Register"/>
</jsp:include>
	<div id="base">
		<h2>Registreer</h2>
		<c:forEach var="error" items="${errors }">
			<p class="alert-danger">${error }</p>
		</c:forEach>
		<form method="POST" action="Controller?action=register">
			<table>
				<tr>
					<td><label for="apb">APB-nummer:</label></td>
					<td><input type="text" id="apb" name="apb"/></td>
				</tr>
				<tr>
					<td><label for="naam">Naam:</label></td>
					<td><input type="text" id="naam" name="naam"/></td>
				</tr>
				<tr>
					<td><label for="leverancier">Leverancier:</label></td>
					<td><select name="leverancier">
							<c:forEach var="leverancier" items="${leveranciers }">
								<option value="${leverancier.naam }">${leverancier.naam }</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="email">E-mail:</label></td>
					<td><input type="text" id="email" name="email"/></<td>
				</tr>
				<tr>
					<td><label for="telefoon">Telefoon:</label></td>
					<td><input type="text" id="telefoon" name="telefoon"/></td>
				</tr>
				<tr>
					<td><label for="gsm">GSM:</label></td>
					<td><input type="text" id="gsm" name="gsm"/></td>
				</tr>
				<tr>
					<td><label for="straat">Straat:</label></td>
					<td><input type="text" id="straat" name="straat"/></td>
				</tr>
				<tr>
					<td><label for="nummer">Nummer:</label></td>
					<td><input type="text" id="nummer" name ="nummer"/></td>
				</tr>
				<tr>
					<td><label for="postcode">Postcode:</label></td>
					<td><input type="text" id="postcode" name="postcode"/></td>
				</tr>
				<tr>
					<td><label for="gemeente">Gemeente:</label></td>
					<td><input type="text" id="gemeente" name="gemeente"/></td>
				</tr>
				<tr>
					<td><label for="paswoord">Paswoord:</label></td>
					<td><input type="password" id="paswoord" name="paswoord"/></td>
				</tr>
				<tr>
					<td><label for="repeatPaswoord">Herhaal Paswoord:</label></td>
					<td><input type="password" id="repeatPaswoord" name="repeatPaswoord"/></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" id="register" value="Registreer" autofocus></td>
				</tr>
			</table>
		</form>
		<a href="Controller?action=home" id="backbutton"><button>Back</button></a>
	</div>
</body>
</html>