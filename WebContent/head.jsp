<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="${param.data }" content="${param.time }">
<title>${param.title }</title>
<link rel="stylesheet" type="text/css" href="css/style.css" title="default">
<link rel="stylesheet" type="text/css" href="css/reset.css" title="default">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="js/chat.js"></script>

</head>
<body onload="onload()">
<div id="container">
	<header>
		<img src="images/Cross_1.png" alt="logo"/>
		<nav>
			<ul>				
				<c:choose>
					<c:when test="${apotheek != null}">
					<li class="dropdown">
						<a href="javascript:void(60)" class="dropbtn">Menu</a>
							<div class="dropdown-content">
								<a href="Controller?action=home">Home</a>
								<a href="Controller?action=stock">Stock</a>
								<a href="Controller?action=matches">Matches</a>
								<a href="Controller?action=history">History</a>
							</div>
					<li id=${param.chat }><a href="Controller?action=chat"><img class="chatIcon" src="images/chatIconIdle.png"></img></a></li>
					<li id=${param.profile }><a href="Controller?action=profile"><img class="profilePicture" src="uploadedImages/${apotheek.fileName }"></img></a></li>
					<li id=${param.logout } class="userOptions"><a href="Controller?action=logout"><span>${apotheek.naam }</span> <br> Log out</a></li>
					</c:when>
				</c:choose>
			</ul>
		</nav>
	</header>
</div>
	