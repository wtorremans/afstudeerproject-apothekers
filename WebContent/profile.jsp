<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<jsp:include page="head.jsp">
	<jsp:param name="title" value="MedMatcher - Profile"/>
</jsp:include>
<script>
	
	$(document).ready(function() {
		$("#changePassword").click(function() {
			$(".password").fadeIn();
		});
		$("#closePassword").click(function() {
			$(".password").fadeOut();
		});
	});
	</script>
<c:choose>	
	<c:when test="${apotheek == null}">
		<div id="base">
        	<h1>Login</h1>
        	<c:forEach var="error" items="${errors }">
            	<p class="alert-danger">${error }</p>
        	</c:forEach>
        	<form method="POST" action="Controller?action=login">
            	<table>
					<tr>
						<td><label for="apb">APB-nummer:</label></td>
						<td><input type="text" id="apb" name="apb" placeholder="APB-nummer" autofocus/></td>
					</tr>
					<tr>
						<td><label for="paswoord">Paswoord:</label></td>
						<td><input type="password" id="paswoord" name="paswoord" placeholder="Paswoord"/></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" id="LogIn" value="Log In" autofocus/></td>
					</tr>
				</table>
        	</form>
       		<a href="Controller?action=registrationPage" id="registrationbutton"><button>Registreer</button></a>
        </div>
	</c:when>
	<c:otherwise>
	<c:forEach var="error" items="${errors }">
			<p class="alert-danger">${error }</p>
		</c:forEach>
		<div id="changeProfile">
		<form method="POST" action="Controller?action=updateProfile">
			<table id="profileTable">
				<tr>
					<td><label for="apb">APB-nummer: </label></td>
					<td><input type="text" name="apb" id="apb" value="${apotheek.apb }" readonly/></td>
				</tr>
				<tr>
					<td><label for="naam">Naam: </label></td>
					<td><input type="text" name="naam" id="naam" value="${apotheek.naam }"/></td>
				</tr>
				<tr>
					<td><label for="leverancier">Leverancier:</label></td>
					<td><select name="leverancier">
							<option value="${apotheek.leverancier }">${apotheek.leverancier}</option>
							<c:forEach var="leverancier" items="${leveranciers }">
								<option value="${leverancier.naam }">${leverancier.naam }</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="email">E-mail: </label></td>
					<td><input type="text" name="email" id="email" value="${apotheek.email }"/></td>
				</tr>
				<tr>
					<td><label for="telefoon">Telefoon: </label></td>
					<td><input type="text" name="telefoon" id="telefoon" value="${apotheek.telefoon }"/></td>
				</tr>
				<tr>
					<td><label for="gsm">GSM: </label></td>
					<td><input type="text" id="gsm" name="gsm" value="${apotheek.gsm }"/></td>
				</tr>
				<tr>
					<td><label for="straat">Straat: </label></td>
					<td><input type="text" id="straat" name="straat" value="${apotheek.straat }"/></td>
				</tr>
				<tr>
					<td><label for="nummer">Nummer: </label></td>
					<td><input type="text" id="nummer" name="nummer" value="${apotheek.nummer }"/></td>
				</tr>
				<tr>
					<td><label for="postcode">Postcode: </label></td>
					<td><input type="text" id="postcode" name="postcode" value="${apotheek.postcode }"/></td>
				</tr>
				<tr>
					<td><label for="gemeente">Gemeente: </label></td>
					<td><input type="text" id="gemeente" name="gemeente" value="${apotheek.gemeente }"/></td>
				</tr>	
				<tr>
					<td></td>
					<td><input type="submit" id="update" value="Opslaan" autofocus></td>
				</tr>
			</table>
		</form>
		
		<p id="changePassword">Verander Wachtwoord</p>
			<form class="password" method="POST" action="Controller?action=updatePassword">
				<table>
					<tr>
						<td><label for="nieuwPaswoord">Nieuw Paswoord:</label></td>
						<td><input type="password" id="nieuwPaswoord" name="nieuwPaswoord" value=""/></td>
					</tr>
					<tr>
						<td><label for="herhaalPaswoord">Herhaal Paswoord:</label></td>
						<td><input type="password" id="herhaalPaswoord" name="herhaalPaswoord" value=""/></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" id="update" value="Verander Wachtwoord" autofocus></td>
					</tr>
				</table>
				<p id="closePassword">Sluiten</p>
			</form>	
			
		</div>	
		<div id="profilePicture">
			<img src="uploadedImages/${apotheek.fileName }"></img>
			<form method="POST" action="Controller?action=uploadPicture" method="post" enctype="multipart/form-data">
				<p><label for="foto">Verander foto: </label></p>
				<p><input type="file" id="foto" name="foto"/></p>
				<p><input type="submit" id="changeFoto" value="Opslaan" autofocus/></p>
			</form>
		</div>
	</c:otherwise>
</c:choose>
</body>
</html>