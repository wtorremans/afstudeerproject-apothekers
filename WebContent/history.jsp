<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page="head.jsp">
	<jsp:param name="title" value="MedMatcher - History"/>
	<jsp:param name="history" value="actual"/>
</jsp:include>

<c:choose>	
	<c:when test="${apotheek == null}">
		<div id="base">
        	<h1>Login</h1>
        	<c:forEach var="error" items="${errors }">
            	<p class="alert-danger">${error }</p>
        	</c:forEach>
        	<form method="POST" action="Controller?action=login">
            	<table>
					<tr>
						<td><label for="apb">APB-nummer:</label></td>
						<td><input type="text" id="apb" name="apb" placeholder="APB-nummer"/></td>
					</tr>
					<tr>
						<td><label for="paswoord">Paswoord:</label></td>
						<td><input type="password" id="paswoord" name="paswoord"/></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" id="LogIn" value="Log In" autofocus/></td>
					</tr>
				</table>
        	</form>
       		<a href="Controller?action=registrationPage" id="registrationbutton"><button>Registreer</button></a>
        </div>
	</c:when>
	<c:otherwise>
	<div id="searchHistory">
		<form method="POST" action="Controller?action=GetMatchesAPB">
			<p><label for="apb">Zoek op APB-nummer:<span unselectable="on">.</span>  </label><input type="text" id="apb" name="apb"/><input type="submit" id="zoek" value="Zoek"/></p>
		</form>
			<h4>OF</h4>
		<form method="POST" action="Controller?action=GetMatchesCNK">
			<p><label for="cnk">Zoek op CNK-nummer: </label><input type="text" id="cnk" name="cnk"/><input type="submit" id="zoek" value="Zoek"/></p>
		</form>
	</div>
		<div id="matchesTables" class="matchTableOverview">
            <div id="tableMatches">
                <table id="fullMatches">   
                    <thead>
                        <tr>
                            <th>Match</th>
                            <th>Aangeboden producten</th>
                            <th>Aantal aangeboden</th>
                            <th>Gevraagde producten</th>
                            <th>Aantal gevraagd</th>
                            <th>Opmerking</th>
                            <th>Timestamp</th>
                        </tr>
                    </thead>
                    <c:forEach var="match" items="${matches}">
                    <tbody id="fullMatchInformation">
                        <tr>
                            <td>${ match.match }</td>
                            <td>${ match.productGevraagd }</td>
                            <td>${ match.aangebodenAantal }</td>
                            <td>${ match.productAangeboden }</td>
                            <td>${ match.gevraagdAantal }</td>
                            <td>${ match.opmerkingen }</td>
                            <td>${ match.timestamp }</td>
                        </tr>
                    </tbody>
                    </c:forEach>
                    <caption>Match Geschiedenis</caption>
                </table>
            </div>
            </div>
            
            <a href="Controller?action=history" id="overviewButton"><button>Back to overview</button></a>
	</c:otherwise>
</c:choose>
</body>
</html>