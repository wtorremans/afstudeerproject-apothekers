<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page="head.jsp">
    <jsp:param name="title" value="MedMatcher - Matches"/>
    <jsp:param name="matches" value="actual"/>
    <jsp:param name="data" value="refresh"/>
    <jsp:param name="time" value="300"/>
</jsp:include>
 
<c:choose>
    <c:when test="${apotheek == null }">
    	<div id="base">
        	<h1>Login</h1>
        	<c:forEach var="error" items="${errors }">
            	<p class="alert-danger">${error }</p>
        	</c:forEach>
        	<form method="POST" action="Controller?action=login">
            	<table>
					<tr>
						<td><label for="apb">APB-nummer:</label></td>
						<td><input type="text" id="apb" name="apb" placeholder="APB-nummer" autofocus/></td>
					</tr>
					<tr>
						<td><label for="paswoord">Paswoord:</label></td>
						<td><input type="password" id="paswoord" name="paswoord" placeholder="Paswoord"/></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" id="LogIn" value="Log In" autofocus/></td>
					</tr>
				</table>
        	</form>
       		<a href="Controller?action=registrationPage" id="registrationbutton"><button>Registreer</button></a>
        </div>
    </c:when>
    <c:otherwise>
    	<div id="matchesTables">
            <div id="tableMatches">
                <table class="fullMatches">   
                    <thead>
                        <tr>
                            <th>Match</th>
                            <th>Aangeboden producten</th>
                            <th>Aantal aangeboden</th>
                            <th>Aantal nodig</th>
                            <th>Vervaldatum aangeboden</th>
                            <th>Gevraagde producten</th>
                            <th>Aantal gevraagd</th>
                            <th>Aantal in aanbieding</th>
                            <th>Totale Prijs</th>
                            <th>Contacteer</th>
                            <th>Bevestig</th>
                        </tr>
                    </thead>
                    <c:forEach var="fullMatch" items="${FullMatchLijst}">
                    <tbody id="fullMatchInformation">
                        <tr>
                            <td>${ fullMatch.aanbieder }</td>
                            <td>${ fullMatch.productAangeboden }</td>
                            <td>${ fullMatch.aangebodenAantal }</td>
                            <td>${ fullMatch.aantalNodig }
                            <td>${ fullMatch.vervaldatum }</td>
                            <td>${ fullMatch.productGevraagd }</td>
                            <td>${ fullMatch.gevraagdAantal }</td>
                            <td>${ fullMatch.aantalAanbieding }</td>
                            <td>${ fullMatch.prijsVerschil }</td>
                            <td><button class="contactButton" onclick="openModal(${fullMatch.aanbieder})">Contact</button></td>
                            <td><button class="confirmButton" onclick="openModalConfirm(${fullMatch.aanbieder})">Bevestig</button></td>
                        </tr>
                    </tbody>
                    <div id="modal${fullMatch.aanbieder }">
                        <div class="modal-content">
                        	<span class="close${fullMatch.aanbieder }" onclick="closeModal(${fullMatch.aanbieder})">&times;</span>
                        	<h3>Stuur ${fullMatch.aanbieder} bericht</h3>
                        	<form method="POST" action="Controller?action=createChat&apotheker=${ fullMatch.aanbieder }" id="inputfield">
								<p>
								<textarea name="message" id="message" rows="8">Graag zou ik de volgende ruil willen voorstellen:&#13;&#10;Aangeboden product: ${fullMatch.aantalAanbieding} stuks ${fullMatch.productGevraagd}&#13;&#10;Gevraagd product: ${fullMatch.aantalNodig} stuks ${fullMatch.productAangeboden}.&#13;&#10;U kan me verder contacteren via de chat.</textarea>
								<input type="submit" id="submit" value="Verzend"/></p>
							</form>
                        </div>
                    </div>
                    <div id="modalConfirm${fullMatch.aanbieder }">
                        <div class="modalConfirm-content">
                        	<span class="closeConfirm${fullMatch.aanbieder }" onclick="closeModalConfirm(${fullMatch.aanbieder})">&times;</span>
                        	<h3>Bevestig match met ${fullMatch.aanbieder}</h3>
                        	<form method="POST" action="Controller?action=bevestigMatch">
								<div>
										<p><label for="apb">APB-nummer: </label>
										<input type="text" name="apb" id="apb" value="${fullMatch.aanbieder }" readonly/></p>
										
										<p><label for="aangeboden product">U biedt aan: </label>
										<input type="text" name="aangebodenProduct" id="aangebodenProduct" value="${fullMatch.productGevraagd}" readonly/></p>
									
										<p><label for="Aantal stuks">Aantal stuks: </label>
										<input type="number" name="aantalAangeboden" id="aantalAangeboden" min=0 max="${fullMatch.aantalAanbieding}" value="${fullMatch.aantalAanbieding}"/></p>
									
										<p><label for="apb">U vraagt: </label>
										<input type="text" name="gevraagdProduct" id="gevraagdProduct" value="${fullMatch.productAangeboden }" readonly/></p>
									
										<p><label for="Aantal stuks">Aantal stuks: </label>
										<input type="number" name="aantalGevraagd" id="aantalGevraagd" min=0 max ="${fullMatch.aangebodenAantal }" value="${fullMatch.aangebodenAantal}"/></p>
									
										<p><label for="Extra opmerkingen">Extra: </label>
										<textarea name="extraOpmerkingen" id="extraOpmerkingen" placeholder="Extra opmerkingen"/></textarea></p>
								
										<input type="submit" id="submit" value="Bevestig"/>
								</div>
								
							</form>
                        </div>
                    </div>
                    </c:forEach>
                    <caption>Volledige Match</caption>
                </table>
            </div>
           
            <div id="tableMatches">
                <table>
                    <tr>
                        <th>Match</th>
                        <th>Producten</th>
                        <th>Aantal</th>
                        <th>Vervaldatum</th>
                        <th>Aantal nodig</th>
                        <th>Totale Prijs</th>
                        <th>Contacteer</th>
                        <th>Bevestig</th>
                    </tr>
                    <c:forEach var="match" items="${VraagMatchLijst}">
                    <tr>
                        <td>${ match.aanbieder }</td>
                        <td>${ match.productAangeboden }</td>
                        <td>${ match.aangebodenAantal }</td>
                        <td>${ match.vervaldatum }</td>
                        <td>${ match.aantalNodig }</td>
                        <td>${ match.prijsVerschil }</td>
                        <td><button class="contactButton" onclick="openModal(${match.aanbieder})">Contact</button></td>
                        <td><button class="confirmButton" onclick="openModalConfirm(${match.aanbieder})">Bevestig</button></td>
                    </tr>
                    <div id="modal${match.aanbieder}">
                        <div class="modal-content">
                        	<span class="close${match.aanbieder }" onclick="closeModal(${match.aanbieder})">&times;</span>
                        	<h3>Stuur ${match.aanbieder} bericht</h3>
                        	<form method="POST" action="Controller?action=createChat&apotheker=${ match.aanbieder }" id="inputfield">
								<p>
								<textarea name="message" id="message" rows="8">Ik ben ge�nteresseerd in het volgende product:&#13;&#10;${match.aantalNodig} stuks ${match.productAangeboden}&#13;&#10;Ik ben bereid u cash te betalen of andere medicijnen in de plaats te geven.&#13;&#10;U kan me verder contacteren via de chat.</textarea>
								<input type="submit" id="submit" value="Verzend"/></p>
							</form>
                        </div>
                    </div>
                    <div id="modalConfirm${match.aanbieder }">
                        <div class="modalConfirm-content">
                        	<span class="closeConfirm${match.aanbieder }" onclick="closeModalConfirm(${match.aanbieder})">&times;</span>
                        	<h3>Bevestig match met ${match.aanbieder}</h3>
                        	<form method="POST" action="Controller?action=bevestigMatch">
								<div>
										<p><label for="apb">APB-nummer: </label>
										<input type="text" name="apb" id="apb" value="${match.aanbieder }" readonly/></p>
										
										<p><label for="aangeboden product">U biedt aan: </label>
										<input type="text" name="aangebodenProduct" id="aangebodenProduct" value="" /></p>
									
										<p><label for="Aantal stuks">Aantal stuks: </label>
										<input type="number" name="aantalAangeboden" id="aantalAangeboden" min=0 value=""/></p>
									
										<p><label for="apb">U vraagt: </label>
										<input type="text" name="gevraagdProduct" id="gevraagdProduct" value="${match.productAangeboden }" readonly/></p>
									
										<p><label for="Aantal stuks">Aantal stuks: </label>
										<input type="number" name="aantalGevraagd" id="aantalGevraagd" min=0 max ="${match.aangebodenAantal }" value="${match.aangebodenAantal}"/></p>
									
										<p><label for="Extra opmerkingen">Extra: </label>
										<textarea name="extraOpmerkingen" id="extraOpmerkingen" placeholder="Extra opmerkingen"/></textarea></p>
								
				
								</div>
								<input type="submit" id="submit" value="Bevestig"/>
							</form>
                        </div>
                    </div>
                    </c:forEach>
                    <caption>Vraag Match (Producten die je nodig hebt en andere apothekers op overschot hebben)</caption>
                </table>
            </div>
           
            <div id="tableMatches">
                <table>
                    <tr>
                        <th>Match</th>
                        <th>Producten</th>
                        <th>Aantal</th>
                        <th>Aantal op overschot</th>
                        <th>Totale Prijs</th>
                        <th>Contacteer</th>
                        <th>Bevestig</th>
                    </tr>
                    <c:forEach var="match" items="${AanbodMatchLijst}">
                    <tr>
                        <td>${ match.vrager }</td>
                        <td>${ match.productGevraagd }</td>
                        <td>${ match.gevraagdAantal }</td>
                        <td>${ match.aantalAanbieding }</td>
                        <td>${ match.prijsVerschil }</td>
                        <td><button class="contactButton" onclick="openModal(${match.vrager})">Contact</button></td>
                        <td><button class="confirmButton" onclick="openModalConfirm(${match.vrager})">Bevestig</button></td>
                    </tr>
                    <div id="modal${match.vrager }">
                        <div class="modal-content">
                        	<span class="close${match.vrager }" onclick="closeModal(${match.vrager})">&times;</span>
                        	<h3>Stuur ${match.vrager} bericht</h3>
                        	<form method="POST" action="Controller?action=createChat&apotheker=${ match.vrager }" id="inputfield">
								<p>
								<textarea name="message" id="message" rows="8">Ik heb gezien dat u volgend product nodig heeft:&#13;&#10;Aangeboden product: ${match.aantalAanbieding} stuks ${match.productGevraagd}&#13;&#10;U kan cash betalen of andere producten in de plaats geven.&#13;&#10;U kan me verder contacteren via de chat.</textarea>
								<input type="submit" id="submit" value="Verzend"/></p>
							</form>
                        </div>
                    </div>
                    <div id="modalConfirm${match.vrager }">
                        <div class="modalConfirm-content">
                        	<span class="closeConfirm${match.vrager }" onclick="closeModalConfirm(${match.vrager})">&times;</span>
                        	<h3>Bevestig match met ${match.vrager}</h3>
                        	<form method="POST" action="Controller?action=bevestigMatch">
								<div>
										<p><label for="apb">APB-nummer: </label>
										<input type="text" name="apb" id="apb" value="${match.vrager }" readonly/></p>
										
										<p><label for="aangeboden product">U biedt aan: </label>
										<input type="text" name="aangebodenProduct" id="aangebodenProduct" value="${match.productGevraagd}" readonly/></p>
									
										<p><label for="Aantal stuks">Aantal stuks: </label>
										<input type="number" name="aantalAangeboden" id="aantalAangeboden" min=0 max="${match.aantalAanbieding}" value="${match.aantalAanbieding}"/></p>
									
										<p><label for="apb">U vraagt: </label>
										<input type="text" name="gevraagdProduct" id="gevraagdProduct" value="" /></p>
									
										<p><label for="Aantal stuks">Aantal stuks: </label>
										<input type="number" name="aantalGevraagd" id="aantalGevraagd" min=0 " value=""/></p>
									
										<p><label for="Extra opmerkingen">Extra: </label>
										<textarea name="extraOpmerkingen" id="extraOpmerkingen" placeholder="Extra opmerkingen"/></textarea></p>
								
				
								</div>
								<input type="submit" id="submit" value="Bevestig"/>
							</form>
                        </div>
                    </div>
                    </c:forEach>
                    <caption>Aanbod Match (Producten die je op overschot hebt en andere apothekers nodig hebben)</caption>
                </table>
            </div>
            <a href="Controller?action=matches"><button id="refresh"><img src="images/refresh.png"/></button></a>
    	</div>
    </c:otherwise>
</c:choose>
<script>

// When the user clicks on the button, open the modal 
function openModal(id) {
	var modal = document.getElementById("modal"+id);
	modal.style.display = "block";
}

function closeModal(id) {
	var modal = document.getElementById("modal"+id);
	modal.style.display = "none";
}

function openModalConfirm(id) {
	var modal = document.getElementById("modalConfirm"+id);
	modal.style.display = "block";
}

function closeModalConfirm(id) {
	var modal = document.getElementById("modalConfirm"+id);
	modal.style.display = "none";
}

</script>

</body>
</html>