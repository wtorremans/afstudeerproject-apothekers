
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<jsp:include page="head.jsp">
	<jsp:param name="title" value="MedMatcher - Login"/>
</jsp:include>

<c:choose>
	<c:when test="${apotheek == null }">
		<div id="base">
        	<h1>Login</h1>
        	<c:forEach var="error" items="${errors }">
            	<p class="alert-danger">${error }</p>
        	</c:forEach>
        	<form method="POST" action="Controller?action=login">
            	<table>
					<tr>
						<td><label for="apb">APB-nummer:</label></td>
						<td><input type="text" id="apb" name="apb" placeholder="APB-nummer" autofocus/></td>
					</tr>
					<tr>
						<td><label for="paswoord">Paswoord:</label></td>
						<td><input type="password" id="paswoord" name="paswoord" placeholder="Paswoord"/></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" id="LogIn" value="Log In" autofocus/></td>
					</tr>
				</table>
        	</form>
       		<a href="Controller?action=registrationPage" id="registrationbutton"><button>Registreer</button></a>
        </div>
	</c:when>
	<c:otherwise>
		<p>Je bent al ingelogd, apotheker ${apotheek.naam}!</p>
	</c:otherwise>
</c:choose>
</body>
</html>