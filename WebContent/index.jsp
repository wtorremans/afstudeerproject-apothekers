<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<jsp:include page="head.jsp">
	<jsp:param name="title" value="MedMatcher - Home"/>
	<jsp:param name="home" value="actual"/>
</jsp:include>

<c:choose>	
	<c:when test="${apotheek == null}">
		<div id="base">
        	<h1>Login</h1>
        	<c:forEach var="error" items="${errors }">
            	<p class="alert-danger">${error }</p>
        	</c:forEach>
        	<form method="POST" action="Controller?action=login">
            	<table>
					<tr>
						<td><label for="apb">APB-nummer:</label></td>
						<td><input type="text" id="apb" name="apb" placeholder="APB-nummer" autofocus/></td>
					</tr>
					<tr>
						<td><label for="paswoord">Paswoord:</label></td>
						<td><input type="password" id="paswoord" name="paswoord" placeholder="Paswoord"/></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" id="LogIn" value="Log In" autofocus/></td>
					</tr>
				</table>
        	</form>
       		<a href="Controller?action=registrationPage" id="registrationbutton"><button>Registreer</button></a>
        </div>
	</c:when>
	<c:otherwise>
		<div id="guidelines">
			<form method="POST" action="Controller?action=matches">
				<h1>Guidelines</h1>
				<p>This application, designed by Wout Torremans and Ruben Schroyen, gives you the ability to find other apothecaries with whom you can trade drugs. </p>
				<p>To set up your stock, go to "Stock" and add your desired or unwanted drugs to your search options. Just search by CNK-number to get the drugs you need.</p>
				<p>After setting up your stock, you can find a match by going to "Matches" or by pressing the button below. You will then get a list of all matches, the amount of money you can save and the amount of matched products.</p>
				<p>In order for others to know who you are, please keep your profile up to date. You can do this by pressing your profile picture.</p>
				<p>In case you want to contact a previous match, you can do so by clicking on the chat icon. You will find all your previous chatlogs there.</p>
				<p>It is also possible to find all your previous matches and trades by going to "History". This is useful for legal purposes.</p>
				<p>Enjoy the application!</p>
				<p><input type="submit" id="zoekMatches" value="Zoek matches"></p>
			</form>
		</div>
	</c:otherwise>
</c:choose>
</body>
</html>