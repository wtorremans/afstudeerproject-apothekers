<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<jsp:include page="head.jsp">
	<jsp:param name="title" value="MedMatcher - Stock"/>
	<jsp:param name="stock" value="actual"/>
</jsp:include>

<c:choose>	
	<c:when test="${apotheek == null}">
		<div id="base">
        	<h1>Login</h1>
        	<c:forEach var="error" items="${errors }">
            	<p class="alert-danger">${error }</p>
        	</c:forEach>
        	<form method="POST" action="Controller?action=login">
            	<table>
					<tr>
						<td><label for="apb">APB-nummer:</label></td>
						<td><input type="text" id="apb" name="apb" placeholder="APB-nummer" autofocus/></td>
					</tr>
					<tr>
						<td><label for="paswoord">Paswoord:</label></td>
						<td><input type="password" id="paswoord" name="paswoord" placeholder="Paswoord"/></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" id="LogIn" value="Log In" autofocus/></td>
					</tr>
				</table>
        	</form>
       		<a href="Controller?action=registrationPage" id="registrationbutton"><button>Registreer</button></a>
        </div>
	</c:when>
	<c:otherwise>
	<div id="addProduct">
		<h3>Zoek product</h3>
		<form method="POST" action="Controller?action=zoekProduct">
			<table>
				<tr>
					<td><label for="CNK">CNK-nummer:</label></td>
					<td><input type="text" name="cnk" id="cnk" value="${product.code }"/>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" id="zoek" value="Zoek" autofocus></td>
				</tr>	
			</table>
			
		</form>
		<br>
		<h3>Product details</h3>
		<c:forEach var="error" items="${errors }">
			<p class="alert-danger">${error }</p>
		</c:forEach>
		<form method="POST" action="Controller?action=voegProductToe">
			<table>
				<tr>
					<td><label for="cnk">CNK-nummer: </label></td>
					<td><input type="text" name="cnkResult" id="cnkResult" value="${product.code }" readonly/></td>
				</tr>
				<tr>
					<td><label for="beschrijving">Beschrijving: </label></td>
					<td><input type="text" name="description" id="description" value="${product.description }" readonly/></td>
				</tr>
				<tr>
					<td><label for="serviceCategory">Service Category: </label></td>
					<td><input type="text" name="serviceCategory" id="serviceCategory" value="${product.serviceCategory }" readonly/></td>
				</tr>
				<tr>
					<td><label for="prijs">Prijs: </label></td>
					<td><input type="text" name="price" id="price" value="${product.price }" readonly/></td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						<select name="typeProduct">
    						<option value="overschot">Overschot</option>
    						<option value="teKort">Te kort</option>
  						</select>
  					</td>
  				</tr>
  				<tr>
  					<td><label for="aantal">Aantal: </label></td>
  					<td><input type="number" min=1 name="aantal" id="aantal"/></td>
  				</tr>
  				<tr>
  					<td><label for="vervaldatum">Vervaldatum: </label></td>
  					<td><input type="String" name="date" id="date"/></td>
  				</tr>
  				<tr>
  					<td></td>
  					<td><input type="submit" id="opslaan" value="Opslaan" autofocus/></td>
  				</tr>
			</table>
		</form>	
	</div>
	<div class="productList">
		<table>	
			<thead>
			<tr>
				<th>CNK-nummer</th>
				<th>Aantal</th>
				<th>Prijs</th>
				<th>Delete?</th>
			</tr>
			</thead>
			<tbody id="tekort">
				<c:forEach var="tekort" items="${ tekortLijst }">
					<tr>
						<td>${tekort.cnk }</td>
						<td>${tekort.aantal }</td>
						<td>${tekort.prijs }</td>
						<td><a href="Controller?action=deleteTeKort&product=${tekort.cnk }"><button>Delete</button></a></td>
					</tr>
				</c:forEach>
			</tbody>
			<caption>Producten tekort</caption>
		</table>
		<table>	
			<thead>
			<tr>
				<th>CNK-nummer</th>
				<th>Aantal</th>
				<th>Prijs</th>
				<th>Vervaldatum</th>
				<th>Delete?</th>
			</tr>
			</thead>
			<tbody id="overschot">
				<c:forEach var="overschot" items="${ overschotLijst }">
					<tr>
						<td>${overschot.cnk }</td>
						<td>${overschot.aantal }</td>
						<td>${overschot.prijs }</td>
						<td>${overschot.vervalDatum }</td>
						<td><a href="Controller?action=deleteOverschot&product=${overschot.cnk }&vervaldatum=${overschot.vervalDatum}"><button>Delete</button></a></td>
					</tr>
				</c:forEach>
			</tbody>
		<caption>Producten op overschot</caption>
		</table>
	</div>
		
	</c:otherwise>
</c:choose>
</body>
</html>