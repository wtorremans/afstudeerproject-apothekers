package db;

import java.util.Collection;
import java.util.Properties;

import domain.Apotheek;
import domain.Chat;
import domain.Message;

public class ChatService {
	private ChatRepositoryDB chatRepository;
	
	public ChatService(Properties properties) {
		chatRepository = new ChatRepositoryDB(properties);
	}

	public int getMaxChats() throws Exception {
		return chatRepository.getMaxChats();
	}
	
	public void createChat(Apotheek a1, Apotheek a2, int id) throws Exception {
		chatRepository.createChat(a1, a2, id);
		
	}

	public Collection<Chat> getAllChatsFor(int apb1) throws Exception {
		return chatRepository.getAllChatsFor(apb1);
	}

	public void deleteChat(int id) {
		chatRepository.deleteChat(id);
	}

	public Chat getChat(Apotheek gebruiker, Apotheek match) {
		return chatRepository.getChat(gebruiker, match);
	}

	public void sendMessage(String text, int senderApb, int receiver, int chat) {
		chatRepository.sendMessage(text, senderApb, receiver, chat);
	}

	public Collection<Message> getMessages(int id) throws Exception {
		return chatRepository.getMessages(id);
	}
	
	public Apotheek getApotheek(int apb) {
		return chatRepository.get(apb);
	}
}
