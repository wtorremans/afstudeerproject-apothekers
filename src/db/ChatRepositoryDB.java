package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;
import domain.Apotheek;
import domain.Chat;
import domain.Message;

public class ChatRepositoryDB {
	private PreparedStatement statement;
	private Connection connection;
	private Properties properties;
	
	public ChatRepositoryDB(Properties properties) {
		try {
			Class.forName("org.postgresql.Driver");
			setProperties(properties);
		} catch(Exception e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}

    public int getMaxChats() throws Exception {
    	ResultSet result = null;
    	int id = 0;
		String sql = "SELECT MAX(\"ID\") FROM \"Afstudeerproject\".\"Chat\"";
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());

			statement = connection.prepareStatement(sql);
			result = statement.executeQuery();
			while(result.next()) {
				id = result.getInt("max");

			}
			return id;
		} catch(SQLException e) {
			throw new Exception(e.getMessage());
		} finally {
			closeConnection();
		}
    }
    
	public void createChat(Apotheek a1, Apotheek a2, int id) throws Exception {
		ArrayList<Chat> chats = getAllChatsFor(a1.getApb());
		
		for (Chat c : chats) {
			if ((c.getA1().getApb() == a1.getApb() && c.getA2().getApb() == a2.getApb()) || (c.getA1().getApb() == a2.getApb() && c.getA2().getApb() == a1.getApb())) {
				return;
			} 
		}
		try {
			String sql = "INSERT INTO \"Afstudeerproject\".\"Chat\" VALUES(?, ?, ?)";
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			
			statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			statement.setInt(2, a1.getApb());
			statement.setInt(3, a2.getApb());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		} finally {
			closeConnection();
		}
	}

	public ArrayList<Chat> getAllChatsFor(int apb) throws Exception {
		ResultSet result = null;
		ArrayList<Chat> chatLijst = new ArrayList<Chat>();
		String sql = "SELECT * FROM \"Afstudeerproject\".\"Chat\" WHERE \"Apotheker1\" = ? OR \"Apotheker2\" = ?";
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());

			statement = connection.prepareStatement(sql);
			statement.setInt(1, apb);
			statement.setInt(2, apb);
			result = statement.executeQuery();
			while(result.next()) {
				int id = result.getInt("ID");
				int apb1 = result.getInt("Apotheker1");
				int apb2 = result.getInt("Apotheker2");
				Apotheek a1 = get(apb1);
				Apotheek a2 = get(apb2);
				Chat chat = new Chat(id, a1, a2);
				chatLijst.add(chat);
			}
			return chatLijst;
		} catch(SQLException e) {
			throw new Exception(e.getMessage());
		} finally {
			closeConnection();
		}
	}

	public void deleteChat(int id) {
		String sql = "DELETE FROM \"Afstudeerproject\".\"Messages\" WHERE \"ChatID\" = ?; DELETE FROM \"Afstudeerproject\".\"Chat\" WHERE \"ID\" = ?; ";
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			statement.setInt(2, id);
			statement.execute();
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		} finally {
			closeConnection();
		}
	}

	public Chat getChat(Apotheek gebruiker, Apotheek match) {
		Chat c = null;
		ResultSet result = null;
		
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			String sql = "SELECT * FROM \"Afstudeerproject\".\"Chat\" WHERE (\"Apotheker1\"= ? AND \"Apotheker2\" = ?) OR (\"Apotheker1\"= ? AND \"Apotheker2\" = ?)" ;
			statement = connection.prepareStatement(sql);
			statement.setInt(1, gebruiker.getApb());
			statement.setInt(2, match.getApb());
			statement.setInt(3, match.getApb());
			statement.setInt(4, gebruiker.getApb());
			result = statement.executeQuery();
			if (result.next()) {
				int chatId = result.getInt("ID");
				int apotheker1apb = result.getInt("Apotheker1");
				int apotheker2apb = result.getInt("Apotheker2");
				Apotheek a1 = get(apotheker1apb);
				Apotheek a2 = get(apotheker2apb);
				c = new Chat(chatId, a1, a2);
				return c;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		} finally {
			closeConnection();
		}
	}

	public void sendMessage(String text, int senderApb, int receiver, int chat) {
		
		String sql = "INSERT INTO \"Afstudeerproject\".\"Messages\" VALUES(?, ?, ?, ?, CURRENT_TIMESTAMP)";
		
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			
			statement = connection.prepareStatement(sql);
			statement.setInt(1, chat);
			statement.setInt(2, senderApb);
			statement.setInt(3, receiver);
			statement.setString(4, text);

			statement.executeUpdate();
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		} finally {
			closeConnection();
		}
	}

	public Collection<Message> getMessages(int id) throws Exception {
		ResultSet result = null;
		Collection<Message> messages = new ArrayList<Message>();
		String sql = "SELECT * FROM \"Afstudeerproject\".\"Messages\" WHERE \"ChatID\" = ? ORDER BY \"Time Send\"";
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());

			statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			result = statement.executeQuery();
			while(result.next()) {
				int senderApb = result.getInt("Sender");
				int receiverApb = result.getInt("Receiver");
				String message = result.getString("Message");
				Timestamp time = result.getTimestamp("Time send");
				Apotheek sender = get(senderApb);
				Apotheek receiver = get(receiverApb);

				Message m = new Message(message, sender, receiver, time);
				messages.add(m);
			}
			return messages;
		} catch(SQLException e) {
			throw new Exception(e.getMessage());
		} finally {
			closeConnection();
		}
	}
	
	public Apotheek get(int apb) {
		Apotheek a = null;
		ResultSet result = null;
		
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			String sql = "SELECT * FROM \"Afstudeerproject\".\"Apothekers\" WHERE \"APB-nummer\"=?";
			statement = connection.prepareStatement(sql);
			statement.setInt(1, apb);
			result = statement.executeQuery();
			if (result.next()) {
				int apbResult = Integer.parseInt(result.getString("APB-nummer"));
				String paswoordResult = result.getString("Paswoord");
				String emailResult = result.getString("E-mail");
				String naamResult = result.getString("Naam");
				String telefoonResult = result.getString("Telefoon");
				String gsmResult = result.getString("GSM-nummer");
				String straatResult = result.getString("Straat");
				String gemeenteResult = result.getString("Gemeente");
				int nummerResult = Integer.parseInt(result.getString("Nummer"));
				int postcodeResult = Integer.parseInt(result.getString("Postcode"));
				String fileNameResult = result.getString("Profielfoto");
				int leverancierId = result.getInt("Leverancier");
				String leverancier = getLeverancier(leverancierId);
				a = new Apotheek(apbResult, paswoordResult, emailResult, telefoonResult, gsmResult, straatResult, gemeenteResult, naamResult, nummerResult, postcodeResult, fileNameResult, leverancier);
				return a;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		} finally {
			closeConnection();
		}
	}
	
	public String getLeverancier(int id) {
		ResultSet result = null;
		
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			String sql = "SELECT * FROM \"Afstudeerproject\".\"Leveranciers\" WHERE \"ID\"=?";
			statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			result = statement.executeQuery();
			if (result.next()) {
				String naam = result.getString("Naam");
				return naam;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		} finally {
			closeConnection();
		}
	}
	
	private Properties getProperties() {
		return properties;
	}
	
	private void setProperties(Properties properties) {
		this.properties = properties;
	}
	
	public void closeConnection() {
		try {
			statement.close();
			connection.close();
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}
	
}
