package db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import domain.Apotheek;
import domain.Leverancier;

public class ApotheekService {
	private ApotheekRepositoryDB apotheekRepository;
	
	public ApotheekService(Properties properties) {
		apotheekRepository = new ApotheekRepositoryDB(properties);
	}
	
	public Apotheek update(int apb, String paswoord, String email, String telefoon, String gsm, String straat, String gemeente, String naam, int nummer, int postcode, String fileName, String leverancier) throws Exception {
		return apotheekRepository.update(apb, paswoord, email, telefoon, gsm, straat, gemeente, naam, nummer, postcode, fileName, leverancier);
	}
	public Apotheek getApotheekIfAuthenticated(int apb, String paswoord) throws SQLException {
		return apotheekRepository.getApotheekIfAuthenticated(apb, paswoord);
	}

	public void add(Apotheek a) {
		apotheekRepository.add(a);
		
	}

	public Apotheek getApotheek(int apb) {
		return apotheekRepository.get(apb);
	}

	public Apotheek updatePassword(int apb, String paswoord) throws Exception {
		// TODO Auto-generated method stub
		return apotheekRepository.updatePassword(apb, paswoord);
	}
	
	public Collection<Leverancier> getLeveranciers() {
		return apotheekRepository.getLeveranciers();
	}
}
