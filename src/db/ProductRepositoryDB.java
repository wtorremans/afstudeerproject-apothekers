package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import domain.Apotheek;
import domain.Product;
import domain.Stock;

public class ProductRepositoryDB {
	private PreparedStatement statement;
	private PreparedStatement statement2;
	private PreparedStatement statement3;
	private Connection connection;
	private Properties properties;
	
	public ProductRepositoryDB(Properties properties) {
		try {
			Class.forName("org.postgresql.Driver");
			setProperties(properties);
		} catch(Exception e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}

	public Product getProduct(String cnk) {
		Product p = null;
		ResultSet result = null;
		
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			String sql = "SELECT * FROM \"Afstudeerproject\".\"Testproducten\" WHERE \"ProductID\"=?";
			statement = connection.prepareStatement(sql);
			statement.setString(1, cnk);
			result = statement.executeQuery();
			if (result.next()) {
				String productId = result.getString("ProductID");
				String description = result.getString("Product Description");
				String serviceCategory = result.getString("Service Category");
				double price = Double.parseDouble(result.getString("Price (in USD)"));

				p = new Product(productId, description, serviceCategory, price);
				return p;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		} finally {
			closeConnection();
		}
	}

	public void addProduct(Apotheek a, Product p, int aantal, String typeProduct, String datum) throws Exception {
		ArrayList<Stock> overschotLijst = getAllOverschot(a.getApb());
		ArrayList<Stock> tekortLijst = getAllTekort(a.getApb());
		
		if (typeProduct.equals("overschot")) {
			try {
					for (int i=0; i < overschotLijst.size(); i++)
					{
						if (overschotLijst.get(i).getCnk().equals(p.getCode()) && overschotLijst.get(i).getVervalDatum().equals(datum))
						{
							String sql = "UPDATE \"Afstudeerproject\".\"ProductenOverschot\" SET \"Aantal\"=?, \"Prijs\"=? WHERE \"Product\"=? AND \"Apotheker\"=? ";
							String url = getProperties().getProperty("url");
							connection = DriverManager.getConnection(url, getProperties());
				
							statement = connection.prepareStatement(sql);
							int newAantal = overschotLijst.get(i).getAantal() + aantal;
							statement.setInt(1, newAantal );
							statement.setDouble(2, newAantal*p.getPrice());
							statement.setString(3, p.getCode());
							statement.setInt(4, a.getApb() );
							statement.executeUpdate();
							return;
						}
						
					}
					
					{
						String sql = "INSERT INTO \"Afstudeerproject\".\"ProductenOverschot\" VALUES(?, ?, ?, ?, ?)";
						String url = getProperties().getProperty("url");
						connection = DriverManager.getConnection(url, getProperties());
						
						statement = connection.prepareStatement(sql);
						statement.setInt(1, a.getApb());
						statement.setString(2, p.getCode());
						statement.setInt(3, aantal );
						statement.setDouble(4, p.getPrice()*aantal);
						statement.setString(5, datum);
						statement.executeUpdate();
					}
					
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		} finally {
			closeConnection();
		}
		}
		if (typeProduct.equals("teKort")) {
			try {
				for (int i=0; i < tekortLijst.size(); i++)
				{
					if (tekortLijst.get(i).getCnk().equals(p.getCode()))
					{
						String sql = "UPDATE \"Afstudeerproject\".\"ProductenTekort\" SET \"Aantal\"=?, \"Prijs\"=? WHERE \"Product\"=? AND \"Apotheker\"=? ";
						String url = getProperties().getProperty("url");
						connection = DriverManager.getConnection(url, getProperties());
						
						statement = connection.prepareStatement(sql);
						int newAantal = tekortLijst.get(i).getAantal() + aantal;
						statement.setInt(1, newAantal );
						statement.setDouble(2, newAantal*p.getPrice());
						statement.setString(3, p.getCode());
						statement.setInt(4, a.getApb() );
						statement.executeUpdate();
						return;
					}
				}

				{
					String sql = "INSERT INTO \"Afstudeerproject\".\"ProductenTekort\" VALUES(?, ?, ?, ?)";
					String url = getProperties().getProperty("url");
					connection = DriverManager.getConnection(url, getProperties());
			
					statement = connection.prepareStatement(sql);
					statement.setInt(1, a.getApb());
					statement.setString(2, p.getCode());
					statement.setInt(3, aantal );
					statement.setDouble(4, p.getPrice()*aantal);
					statement.executeUpdate();
			
				}
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		} finally {
			closeConnection();
		}
		}
		
		
	}
	
	public ArrayList<Stock> getAllTekort(int apb) throws Exception {
		ResultSet result = null;
		ArrayList<Stock> tekortLijst = new ArrayList<Stock>();
		String sql = "SELECT * FROM \"Afstudeerproject\".\"ProductenTekort\" WHERE \"Apotheker\" = ?";
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());

			statement = connection.prepareStatement(sql);
			statement.setInt(1, apb);
			result = statement.executeQuery();
			while(result.next()) {
				int a = result.getInt("Apotheker");
				String cnk = result.getString("product");
				int aantal = result.getInt("aantal");
				double prijs = result.getDouble("prijs");
				String datum = null;
				Stock stock = new Stock(a, cnk, aantal, prijs, datum);
				tekortLijst.add(stock);
			}
			return tekortLijst;
		} catch(SQLException e) {
			throw new Exception(e.getMessage());
		} finally {
			closeConnection();
		}
	}
	
	public ArrayList<Stock> getAllOverschot(int apb) throws Exception {
		ResultSet result = null;
		ArrayList<Stock> overschotLijst = new ArrayList<Stock>();
		String sql = "SELECT * FROM \"Afstudeerproject\".\"ProductenOverschot\" WHERE \"Apotheker\" = ?";
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());

			statement = connection.prepareStatement(sql);
			statement.setInt(1, apb);
			result = statement.executeQuery();
			while(result.next()) {
				int a = result.getInt("Apotheker");
				String cnk = result.getString("product");
				int aantal = result.getInt("aantal");
				double prijs = result.getDouble("prijs");
				String datum = result.getString("Vervaldatum");
				Stock stock = new Stock(a, cnk, aantal, prijs, datum);
				overschotLijst.add(stock);
			}
			return overschotLijst;
		} catch(SQLException e) {
			throw new Exception(e.getMessage());
		} finally {
			closeConnection();
		}
	}

	public void deleteTeKort(String product, int apb) {
		String sql = "DELETE FROM \"Afstudeerproject\".\"ProductenTekort\" WHERE \"Product\" = ? AND \"Apotheker\" = ?;";
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			statement = connection.prepareStatement(sql);
			statement.setString(1, product);
			statement.setInt(2, apb);
			statement.execute();
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		} finally {
			closeConnection();
		}
		
	}

	public void deleteOverschot(String product, int apb, String vervaldatum) {
		String sql = "DELETE FROM \"Afstudeerproject\".\"ProductenOverschot\" WHERE \"Product\" = ? AND \"Apotheker\" = ? AND \"Vervaldatum\"=?;";
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			statement = connection.prepareStatement(sql);
			statement.setString(1, product);
			statement.setInt(2, apb);
			statement.setString(3, vervaldatum);
			statement.execute();
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		} finally {
			closeConnection();
		}
		
	}

	public void deleteVervaldatum() {
		String sql = "DELETE FROM \"Afstudeerproject\".\"ProductenOverschot\" WHERE to_date(\"Vervaldatum\", 'DD/MM/YYYY') < CURRENT_DATE;";
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			statement = connection.prepareStatement(sql);
			statement.execute();
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		} finally {
			closeConnection();
		}
	}
	
	private Properties getProperties() {
		return properties;
	}
	
	private void setProperties(Properties properties) {
		this.properties = properties;
	}
	
	public void closeConnection() {
		try {
			statement.close();
			connection.close();
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}
	

	public void deleteFromStockAanbieder(int aanbieder, String aanbod, int aangebodenAantal) throws Exception {
		ResultSet result = null;
		int newAantal = aangebodenAantal;
		ArrayList<Stock> overschotLijst = new ArrayList<Stock>();
		String sql = "SELECT * FROM \"Afstudeerproject\".\"ProductenOverschot\" WHERE \"Apotheker\"=? AND \"Product\"=? ORDER BY \"Vervaldatum\" DESC";
		String sql2 = "UPDATE \"Afstudeerproject\".\"ProductenOverschot\" SET \"Aantal\"=?, \"Prijs\"=? WHERE \"Apotheker\" =? AND \"Product\"=? AND \"Vervaldatum\"=?";
		String sql3 = "DELETE FROM \"Afstudeerproject\".\"ProductenOverschot\" WHERE \"Apotheker\"=? AND \"Product\"=? AND \"Vervaldatum\"=?";
		try { 
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			statement = connection.prepareStatement(sql);
			statement.setInt(1, aanbieder);
			statement.setString(2, aanbod);
			result = statement.executeQuery();
			while(result.next()) {
				int a = result.getInt("Apotheker");
				String cnk = result.getString("product");
				int aantal = result.getInt("aantal");
				double prijs = result.getDouble("prijs");
				String datum = result.getString("Vervaldatum");
				Stock stock = new Stock(a, cnk, aantal, prijs, datum);
				overschotLijst.add(stock);
			}
			for (Stock s: overschotLijst)
			{
				if (s.getAantal() - aangebodenAantal > 0) {
					statement2 = connection.prepareStatement(sql2);
					newAantal = s.getAantal() - aangebodenAantal;
					statement2.setInt(1, newAantal );
					statement2.setDouble(2, getProduct(s.getCnk()).getPrice()*newAantal);
					statement2.setInt(3, aanbieder);
					statement2.setString(4, aanbod);
					statement2.setString(5, s.getVervalDatum());
					statement2.execute();
				}
				else {
					while(newAantal > 0) {
						newAantal = newAantal - s.getAantal();
						statement3 = connection.prepareStatement(sql3);
						statement3.setInt(1, aanbieder );
						statement3.setString(2, aanbod);
						statement3.setString(3, s.getVervalDatum());
						statement3.execute();
					}
				}
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		} 
		finally {
			closeConnection();
		}
		
	}

	public void deleteFromStockVrager(int vrager, String gevraagd, int gevraagdAantal) throws Exception {
		ResultSet result = null;
		int newAantal = gevraagdAantal;
		ArrayList<Stock> tekortLijst = new ArrayList<Stock>();
		String sql = "SELECT * FROM \"Afstudeerproject\".\"ProductenTekort\" WHERE \"Apotheker\"=? AND \"Product\"=?";
		String sql2 = "UPDATE \"Afstudeerproject\".\"ProductenTekort\" SET \"Aantal\"=?, \"Prijs\"=? WHERE \"Apotheker\" =? AND \"Product\"=?";
		String sql3 = "DELETE FROM \"Afstudeerproject\".\"ProductenTekort\" WHERE \"Apotheker\"=? AND \"Product\"=?";
		try { 
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			statement = connection.prepareStatement(sql);
			statement.setInt(1, vrager);
			statement.setString(2, gevraagd);
			result = statement.executeQuery();
			while(result.next()) {
				int a = result.getInt("Apotheker");
				String cnk = result.getString("product");
				int aantal = result.getInt("aantal");
				double prijs = result.getDouble("prijs");
				Stock stock = new Stock(a, cnk, aantal, prijs, null);
				tekortLijst.add(stock);
			}
			for (Stock s: tekortLijst)
			{
				if (s.getAantal() - gevraagdAantal > 0) {
					statement2 = connection.prepareStatement(sql2);
					newAantal = s.getAantal() - gevraagdAantal;
					statement2.setInt(1, newAantal );
					statement2.setDouble(2, getProduct(s.getCnk()).getPrice()*newAantal);
					statement2.setInt(3, vrager);
					statement2.setString(4, gevraagd);
					statement2.execute();
				}
				else {
					statement3 = connection.prepareStatement(sql3);
					statement3.setInt(1, vrager );
					statement3.setString(2, gevraagd);
					statement3.execute();
				}
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		} 
		finally {
			closeConnection();
		}
		
		
	}

	
}
