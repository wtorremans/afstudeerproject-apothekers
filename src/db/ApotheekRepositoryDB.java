package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;
import domain.Apotheek;
import domain.Leverancier;

public class ApotheekRepositoryDB {
	private PreparedStatement statement;
	private Connection connection;
	private Properties properties;
	
	public ApotheekRepositoryDB(Properties properties) {
		try {
			Class.forName("org.postgresql.Driver");
			setProperties(properties);
		} catch(Exception e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}
	
	public Apotheek getApotheekIfAuthenticated(int apb, String paswoord) throws SQLException {
		Apotheek apotheker = null;
		ResultSet result = null;
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			String sql="SELECT * FROM \"Afstudeerproject\".\"Apothekers\" WHERE \"APB-nummer\"=? AND \"Paswoord\"=? ";
			statement = connection.prepareStatement(sql);
			statement.setInt(1, apb);
			statement.setString(2, paswoord);
			result = statement.executeQuery();
			if (result.next()) {
				int apbResult = Integer.parseInt(result.getString("APB-nummer"));
				String paswoordResult = result.getString("Paswoord");
				String emailResult = result.getString("E-mail");
				String naamResult = result.getString("Naam");
				String telefoonResult = result.getString("Telefoon");
				String gsmResult = result.getString("GSM-nummer");
				String straatResult = result.getString("Straat");
				String gemeenteResult = result.getString("Gemeente");
				int nummerResult = Integer.parseInt(result.getString("Nummer"));
				int postcodeResult = Integer.parseInt(result.getString("Postcode"));
				String fileNameResult = result.getString("Profielfoto");
				int leverancierId = result.getInt("Leverancier");
				String leverancier = getLeverancier(leverancierId);
				apotheker = new Apotheek(apbResult, paswoordResult, emailResult, telefoonResult, gsmResult, straatResult, gemeenteResult, naamResult, nummerResult, postcodeResult, fileNameResult, leverancier);
				return apotheker;
			} else {
				return null;
			}
		} finally {
			closeConnection();
		}
	}
	
	public Apotheek update(int apb, String paswoord, String email, String telefoon, String gsm, String straat, String gemeente, String naam, int nummer, int postcode, String fileName, String leverancier) throws Exception {
		int leverancierid = getLeverancierByNaam(leverancier);
		String sql = "UPDATE \"Afstudeerproject\".\"Apothekers\" SET \"Naam\"=?, \"E-mail\"=?, \"Telefoon\"=?, \"GSM-nummer\"=?, \"Straat\"=?, \"Nummer\"=?, \"Postcode\"=?, \"Gemeente\"=?, \"Leverancier\"=?, \"Profielfoto\"=? WHERE \"APB-nummer\" =?";
		try { 
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			statement = connection.prepareStatement(sql);
			
			statement.setString(1, naam);
			statement.setString(2, email);
			statement.setString(3, telefoon);
			statement.setString(4, gsm);
			statement.setString(5, straat);
			statement.setInt(6, nummer);
			statement.setInt(7, postcode);
			statement.setString(8, gemeente);
			statement.setInt(9, leverancierid);
			statement.setString(10, fileName);
			statement.setInt(11, apb);
			statement.execute();
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		} 
		return get(apb);

	}
	
	private int getLeverancierByNaam(String leverancier) {
		ResultSet result = null;
		
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			String sql = "SELECT * FROM \"Afstudeerproject\".\"Leveranciers\" WHERE \"Naam\"=?";
			statement = connection.prepareStatement(sql);
			statement.setString(1, leverancier);
			result = statement.executeQuery();
			if (result.next()) {
				int id = result.getInt("ID");
				return id;
			} else {
				return 1;
			}
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		} 
	}

	public Apotheek updatePassword(int apb, String paswoord) throws Exception {
		String sql = "UPDATE \"Afstudeerproject\".\"Apothekers\" SET \"Paswoord\"=? WHERE \"APB-nummer\" =?";
		String url = getProperties().getProperty("url");
		connection = DriverManager.getConnection(url, getProperties());
		try { 
			statement = connection.prepareStatement(sql);
			statement.setString(1, paswoord);
			statement.setInt(2, apb);
			statement.execute();
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return getApotheekIfAuthenticated(apb, paswoord);
	}


	public void add(Apotheek a) {
		int leverancier = getLeverancierByNaam(a.getLeverancier());
		
		try {
			String sql = "INSERT INTO \"Afstudeerproject\".\"Apothekers\" VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			
			statement = connection.prepareStatement(sql);
			statement.setString(1, a.getNaam());
			statement.setString(2, a.getStraat());
			statement.setInt(3, a.getNummer());
			statement.setInt(4, a.getPostcode());
			statement.setString(5, a.getTelefoon());
			statement.setString(6, a.getGsm());
			statement.setString(7, a.getEmail());
			statement.setString(8, a.getPaswoord());
			statement.setString(9, a.getGemeente());
			statement.setInt(10, a.getApb());
			statement.setInt(11, leverancier);
			statement.setString(12, a.getFileName());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		} finally {
			closeConnection();
		}
		
	}

	public Apotheek get(int apb) {
		Apotheek a = null;
		ResultSet result = null;
		
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			String sql = "SELECT * FROM \"Afstudeerproject\".\"Apothekers\" WHERE \"APB-nummer\"=?";
			statement = connection.prepareStatement(sql);
			statement.setInt(1, apb);
			result = statement.executeQuery();
			if (result.next()) {
				int apbResult = Integer.parseInt(result.getString("APB-nummer"));
				String paswoordResult = result.getString("Paswoord");
				String emailResult = result.getString("E-mail");
				String naamResult = result.getString("Naam");
				String telefoonResult = result.getString("Telefoon");
				String gsmResult = result.getString("GSM-nummer");
				String straatResult = result.getString("Straat");
				String gemeenteResult = result.getString("Gemeente");
				int nummerResult = Integer.parseInt(result.getString("Nummer"));
				int postcodeResult = Integer.parseInt(result.getString("Postcode"));
				String fileNameResult = result.getString("Profielfoto");
				int leverancierId = result.getInt("Leverancier");
				String leverancier = getLeverancier(leverancierId);
				a = new Apotheek(apbResult, paswoordResult, emailResult, telefoonResult, gsmResult, straatResult, gemeenteResult, naamResult, nummerResult, postcodeResult, fileNameResult, leverancier);
				return a;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		} finally {
			closeConnection();
		}
	}
	
	public String getLeverancier(int id) {
		ResultSet result = null;
		
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			String sql = "SELECT * FROM \"Afstudeerproject\".\"Leveranciers\" WHERE \"ID\"=?";
			statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			result = statement.executeQuery();
			if (result.next()) {
				String naam = result.getString("Naam");
				return naam;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		} finally {
			closeConnection();
		}
	}
	
	public Collection<Leverancier> getLeveranciers() {
		Collection<Leverancier> leveranciers = new ArrayList<Leverancier>();
		ResultSet result = null;
		
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			String sql = "SELECT * FROM \"Afstudeerproject\".\"Leveranciers\"";
			statement = connection.prepareStatement(sql);
			result = statement.executeQuery();
			
			while (result.next()) {
				int id = result.getInt("ID");
				String naam = result.getString("Naam");
				String straat = result.getString("Straat");
				int nummer = result.getInt("Nummer");
				int postcode = result.getInt("Postcode");
				String gemeente = result.getString("Gemeente");
				Leverancier leverancier = new Leverancier(id, naam, straat, nummer, postcode, gemeente);
				leveranciers.add(leverancier);
			}
			return leveranciers;
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		} finally {
			closeConnection();
		}
	}
	
	private Properties getProperties() {
		return properties;
	}
	
	private void setProperties(Properties properties) {
		this.properties = properties;
	}
	
	public void closeConnection() {
		try {
			statement.close();
			connection.close();
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}	
}
