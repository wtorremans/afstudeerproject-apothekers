package db;

import java.util.Collection;
import java.util.Properties;

import domain.Apotheek;
import domain.ConfirmedMatch;
import domain.Match;

public class MatchService {
	private MatchRepositoryDB matchRepository;
	
	public MatchService(Properties properties) {
		matchRepository = new MatchRepositoryDB(properties);
	}

	public Collection<Match> getFullMatch(int apb) throws Exception {
		return matchRepository.getFullMatch(apb);
	}

	public Collection<Match> getVraagMatch(int apb) throws Exception {
		return matchRepository.getVraagMatch(apb);
	}

	public Collection<Match> getAanbodMatch(int apb) throws Exception {
		return matchRepository.getAanbodMatch(apb);
	}

	public void addToConfirmedMatches(Match m, Apotheek a, String opmerkingen) {
		matchRepository.addToConfirmedMatches(m, a, opmerkingen);
		
	}

	public Collection<ConfirmedMatch> getConfirmedMatches(int ownAPB, int apb, String CNK) throws Exception {
		return matchRepository.getConfirmedMatches(ownAPB, apb, CNK);
	}
	
	public Collection<ConfirmedMatch> getConfirmedMatches(int apb) throws Exception {
		return matchRepository.getConfirmedMatches(apb);
	}

	
	
}
