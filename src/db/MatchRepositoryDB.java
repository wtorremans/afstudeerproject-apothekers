package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import domain.Apotheek;
import domain.ConfirmedMatch;
import domain.Match;

public class MatchRepositoryDB {
	private PreparedStatement statement;
	private Connection connection;
	private Properties properties;
	
	public MatchRepositoryDB(Properties properties) {
		try {
			Class.forName("org.postgresql.Driver");
			setProperties(properties);
		} catch(Exception e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}

	public Collection<Match> getFullMatch(int apb) throws Exception {
        ResultSet result = null;
        Collection<Match> fullMatchLijst = new ArrayList<Match>();
        String sql = "SELECT t1.\"Product\" AS \"biedt aan\", t1.\"Aanbieder\", t1.\"Aangeboden\", t1.\"Nodig\" AS \"Aantal Nodig\", t1.\"Vervaldatum\", t1.aangebodenPrijs, t2.\"Vrager\", t2.\"Product\" AS \"Vraagt\", t2.\"Nodig\" AS \"Aantal Gevraagd\", t2.\"Aangeboden\" AS \"Aantal Aangeboden\", t2.gevraagdeprijs, (CASE " +
                "                                                                                                                                       WHEN t1.aangebodenprijs is null THEN -t2.gevraagdeprijs " +
                "                                                                                                                                        WHEN t2.gevraagdeprijs is null THEN t1.aangebodenprijs " +
                "                                                                                                                                       ELSE t1.aangebodenprijs - t2.gevraagdeprijs " +
                "                                                                                                                                   END) AS prijsverschil " +
                "FROM " +
                "(SELECT \"Product\", o.\"Apotheker\" AS \"Aanbieder\", o.\"Aantal\" AS \"Aangeboden\", o.\"Prijs\" AS aangebodenPrijs, o.\"Vervaldatum\", k.\"Apotheker\" AS \"Vrager\", k.\"Aantal\" AS \"Nodig\", k.\"Prijs\" " +
                "FROM \"Afstudeerproject\".\"ProductenOverschot\" o INNER JOIN \"Afstudeerproject\".\"ProductenTekort\" k USING(\"Product\") " +
                "WHERE o.\"Apotheker\" <> k.\"Apotheker\" " +
                "AND k.\"Apotheker\" = ? " +
                "ORDER BY o.\"Apotheker\", k.\"Apotheker\") t1 INNER JOIN (SELECT \"Product\", o.\"Apotheker\" AS \"Aanbieder\", o.\"Aantal\" AS \"Aangeboden\", o.\"Prijs\", o.\"Vervaldatum\", k.\"Apotheker\" AS \"Vrager\", k.\"Aantal\" AS \"Nodig\", k.\"Prijs\" AS gevraagdePrijs " +
                "FROM \"Afstudeerproject\".\"ProductenOverschot\" o INNER JOIN \"Afstudeerproject\".\"ProductenTekort\" k USING(\"Product\") " +
                "WHERE o.\"Apotheker\" <> k.\"Apotheker\" " +
                "AND o.\"Apotheker\" = ? " +
                "ORDER BY o.\"Apotheker\", k.\"Apotheker\") t2 ON t1.\"Aanbieder\" = t2.\"Vrager\" " +
                "ORDER BY prijsverschil DESC";
        try {
            String url = getProperties().getProperty("url");
            connection = DriverManager.getConnection(url, getProperties());
            statement = connection.prepareStatement(sql);
            statement.setInt(1, apb);
            statement.setInt(2, apb);
            result = statement.executeQuery();
            while(result.next()) {
                int aanbieder = result.getInt("Aanbieder");
                String productAangeboden = result.getString("biedt aan");
                int aantalAangeboden = result.getInt("Aangeboden");
                double prijsverschil = result.getDouble("Prijsverschil");
                int vrager = result.getInt("vrager");
                String productGevraagd = result.getString("Vraagt");
                int aantalGevraagd = result.getInt("Aantal Gevraagd");
                String vervalDatum = result.getString("Vervaldatum");
                int aantalNodig = result.getInt("Aantal Nodig");
                int aantalAanbieding = result.getInt("Aantal Aangeboden");
                Match m = new Match(productAangeboden, aanbieder, aantalAangeboden, aantalNodig, vervalDatum, vrager, productGevraagd, aantalGevraagd, aantalAanbieding, prijsverschil);
                fullMatchLijst.add(m);
            }
            return fullMatchLijst;
        } catch(SQLException e) {
            throw new Exception(e.getMessage());
        } finally {
            closeConnection();
        }
    }
 
    public Collection<Match> getVraagMatch(int apb) throws Exception {
        ResultSet result = null;
        Collection<Match> vraagMatchLijst = new ArrayList<Match>();
        String sql = "SELECT t1.\"Product\" AS \"biedt aan\", t1.\"Aanbieder\", t1.\"Aangeboden\", t1.\"Nodig\" AS \"Aantal Nodig\", t1.\"Vervaldatum\", t1.aangebodenPrijs, t2.\"Vrager\",     t2.\"Product\" AS \"Vraagt\", t2.\"Nodig\" AS \"Aantal Gevraagd\", t2.\"Aangeboden\" AS \"Aantal Aangeboden\", t2.gevraagdeprijs,  " +
                "   (CASE  " +
                "       WHEN t1.aangebodenprijs is null  " +
                "           THEN -t2.gevraagdeprijs                                                                                                                                             WHEN t2.gevraagdeprijs is null  " +
                "           THEN t1.aangebodenprijs " +
                "       ELSE t1.aangebodenprijs - t2.gevraagdeprijs " +
                "   END) AS prijsverschil " +
                "FROM  " +
                "   (SELECT  \"Product\", o.\"Apotheker\" AS \"Aanbieder\", o.\"Aantal\" AS \"Aangeboden\",  " +
                "          o.\"Prijs\" AS aangebodenPrijs, o.\"Vervaldatum\", k.\"Apotheker\" AS \"Vrager\",               k.\"Aantal\" AS \"Nodig\", k.\"Prijs\" " +
                "   FROM \"Afstudeerproject\".\"ProductenOverschot\" o  " +
                "           INNER JOIN \"Afstudeerproject\".\"ProductenTekort\" k  USING(\"Product\") " +
                "   WHERE o.\"Apotheker\" <> k.\"Apotheker\" AND  k.\"Apotheker\" = ? " +
                "   ORDER BY o.\"Apotheker\", k.\"Apotheker\")   t1  " +
                " " +
                "LEFT JOIN " +
                "   (SELECT \"Product\", o.\"Apotheker\" AS \"Aanbieder\", o.\"Aantal\" AS \"Aangeboden\", " +
                "        o.\"Prijs\", o.\"Vervaldatum\", k.\"Apotheker\" AS \"Vrager\", k.\"Aantal\" AS \"Nodig\",           k.\"Prijs\" AS gevraagdePrijs " +
                "   FROM \"Afstudeerproject\".\"ProductenOverschot\" o  " +
                "           INNER JOIN \"Afstudeerproject\".\"ProductenTekort\" k USING(\"Product\") " +
                "   WHERE o.\"Apotheker\" <> k.\"Apotheker\" AND  o.\"Apotheker\" = ? " +
                "   ORDER BY o.\"Apotheker\", k.\"Apotheker\") " +
                " " +
                " t2 ON t1.\"Aanbieder\" = t2.\"Vrager\" " +
                " WHERE t2.\"Product\" IS NULL " +
                "ORDER BY prijsverschil DESC";
        try {
            String url = getProperties().getProperty("url");
            connection = DriverManager.getConnection(url, getProperties());
 
            statement = connection.prepareStatement(sql);
            statement.setInt(1, apb);
            statement.setInt(2, apb);
            result = statement.executeQuery();
            while(result.next()) {
                int aanbieder = result.getInt("Aanbieder");
                String productAangeboden = result.getString("biedt aan");
                int aantalAangeboden = result.getInt("Aangeboden");
                double prijsverschil = result.getDouble("Prijsverschil");
                int vrager = result.getInt("vrager");
                String productGevraagd = result.getString("Vraagt");
                int aantalGevraagd = result.getInt("Aantal Gevraagd");
                String vervalDatum = result.getString("Vervaldatum");
                int aantalNodig = result.getInt("Aantal Nodig");
                int aantalAanbieding = result.getInt("Aantal Aangeboden");
                Match m = new Match(productAangeboden, aanbieder, aantalAangeboden, aantalNodig, vervalDatum, vrager, productGevraagd, aantalGevraagd, aantalAanbieding, prijsverschil);
                vraagMatchLijst.add(m);
            }
            return vraagMatchLijst;
        } catch(SQLException e) {
            throw new Exception(e.getMessage());
        } finally {
            closeConnection();
        }
    }
 
    public Collection<Match> getAanbodMatch(int apb) throws Exception {
        ResultSet result = null;
        Collection<Match> aanbodMatchLijst = new ArrayList<Match>();
        String sql = "SELECT t1.\"Product\" AS \"biedt aan\", t1.\"Aanbieder\", t1.\"Aangeboden\", t1.\"Nodig\" AS \"Aantal Nodig\", t1.aangebodenPrijs, t2.\"Vrager\",     t2.\"Product\" AS \"Vraagt\", t2.\"Nodig\" AS \"Aantal Gevraagd\", t2.\"Aangeboden\" AS \"Aantal Aangeboden\", t2.gevraagdeprijs,  " +
                "   (CASE  " +
                "       WHEN t1.aangebodenprijs is null  " +
                "           THEN -t2.gevraagdeprijs                                                                                                                                             WHEN t2.gevraagdeprijs is null  " +
                "           THEN t1.aangebodenprijs " +
                "       ELSE t1.aangebodenprijs - t2.gevraagdeprijs " +
                "   END) AS prijsverschil " +
                "FROM  " +
                "   (SELECT  \"Product\", o.\"Apotheker\" AS \"Aanbieder\", o.\"Aantal\" AS \"Aangeboden\",  " +
                "          o.\"Prijs\" AS aangebodenPrijs, o.\"Vervaldatum\", k.\"Apotheker\" AS \"Vrager\",               k.\"Aantal\" AS \"Nodig\", k.\"Prijs\" " +
                "   FROM \"Afstudeerproject\".\"ProductenOverschot\" o  " +
                "           INNER JOIN \"Afstudeerproject\".\"ProductenTekort\" k  USING(\"Product\") " +
                "   WHERE o.\"Apotheker\" <> k.\"Apotheker\" AND  k.\"Apotheker\" = ? " +
                "   ORDER BY o.\"Apotheker\", k.\"Apotheker\")   t1  " +
                " " +
                "RIGHT JOIN " +
                "   (SELECT \"Product\", o.\"Apotheker\" AS \"Aanbieder\", o.\"Aantal\" AS \"Aangeboden\", " +
                "        o.\"Prijs\", o.\"Vervaldatum\", k.\"Apotheker\" AS \"Vrager\", k.\"Aantal\" AS \"Nodig\",           k.\"Prijs\" AS gevraagdePrijs " +
                "   FROM \"Afstudeerproject\".\"ProductenOverschot\" o  " +
                "           INNER JOIN \"Afstudeerproject\".\"ProductenTekort\" k USING(\"Product\") " +
                "   WHERE o.\"Apotheker\" <> k.\"Apotheker\" AND  o.\"Apotheker\" = ? " +
                "   ORDER BY o.\"Apotheker\", k.\"Apotheker\") " +
                " " +
                " t2 ON t1.\"Aanbieder\" = t2.\"Vrager\" " +
                " WHERE t1.\"Product\" IS NULL " +
                "ORDER BY prijsverschil DESC";
        try {
            String url = getProperties().getProperty("url");
            connection = DriverManager.getConnection(url, getProperties());
 
            statement = connection.prepareStatement(sql);
            statement.setInt(1, apb);
            statement.setInt(2, apb);
            result = statement.executeQuery();
            while(result.next()) {
                int aanbieder = result.getInt("Aanbieder");
                String productAangeboden = result.getString("biedt aan");
                int aantalAangeboden = result.getInt("Aangeboden");
                double prijsverschil = result.getDouble("Prijsverschil");
                int vrager = result.getInt("vrager");
                String productGevraagd = result.getString("Vraagt");
                int aantalGevraagd = result.getInt("Aantal Gevraagd");
                int aantalNodig = result.getInt("Aantal Nodig");
                int aantalAanbieding = result.getInt("Aantal Aangeboden");
                Match m = new Match(productAangeboden, aanbieder, aantalAangeboden, aantalNodig, null, vrager, productGevraagd, aantalGevraagd, aantalAanbieding, prijsverschil);
                aanbodMatchLijst.add(m);
            }
            return aanbodMatchLijst;
        } catch(SQLException e) {
            throw new Exception(e.getMessage());
        } finally {
            closeConnection();
        }
    }	
    

	public void addToConfirmedMatches(Match m, Apotheek a, String opmerkingen) {
		String sql = "INSERT INTO \"Afstudeerproject\".\"ConfirmedMatches\" VALUES(?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP, ?)";
		
		try {
			String url = getProperties().getProperty("url");
			connection = DriverManager.getConnection(url, getProperties());
			
			statement = connection.prepareStatement(sql);
			if (m.getAanbieder() == a.getApb())
				statement.setInt(1, m.getVrager());
			else
				statement.setInt(1, m.getAanbieder());
			statement.setString(2, m.getProductAangeboden());
			statement.setInt(3, m.getAangebodenAantal());
			statement.setString(4, m.getProductGevraagd());
			statement.setInt(5, m.getGevraagdAantal());
			statement.setString(6, opmerkingen);
			if (m.getAanbieder() != a.getApb())
				statement.setInt(7, m.getVrager());
			else
				statement.setInt(7, m.getAanbieder());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		} finally {
			closeConnection();
		}
		
	}
    
    private Properties getProperties() {
		return properties;
	}
	
	private void setProperties(Properties properties) {
		this.properties = properties;
	}
	
	public void closeConnection() {
		try {
			statement.close();
			connection.close();
		} catch (SQLException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}
	
	public Collection<ConfirmedMatch> getConfirmedMatches(int apb) throws Exception {
		ResultSet result = null;
        Collection<ConfirmedMatch> confirmedMatches = new ArrayList<ConfirmedMatch>();
        String sql = "SELECT * FROM \"Afstudeerproject\".\"ConfirmedMatches\" WHERE \"APB-match\"=? OR \"APB-match2\"=? ORDER BY \"Timestamp\" DESC";
        try {
            String url = getProperties().getProperty("url");
            connection = DriverManager.getConnection(url, getProperties());
 
            statement = connection.prepareStatement(sql);

            
            statement.setInt(1, apb);
            statement.setInt(2, apb);
            result = statement.executeQuery();
            while(result.next()) {
            	int match = result.getInt("APB-match");
                int match2 = result.getInt("APB-match2");
                int matchResult = 0;
                if (match == apb)
                	matchResult = match2;
                else
                	matchResult = match;
                String productAangeboden = result.getString("ProductAangeboden");
                int aantalAangeboden = result.getInt("AantalAangeboden");
                String productGevraagd = result.getString("ProductenGevraagd");
                int aantalGevraagd = result.getInt("AantalGevraagd");
                String opmerkingen = result.getString("Opmerkingen");
                Timestamp timestamp = result.getTimestamp("Timestamp");
                ConfirmedMatch m = new ConfirmedMatch(productAangeboden, matchResult, aantalAangeboden, productGevraagd, aantalGevraagd, timestamp, opmerkingen);
                confirmedMatches.add(m);
            }
            return confirmedMatches;
        } catch(SQLException e) {
            throw new Exception(e.getMessage());
        } finally {
            closeConnection();
        }
	}

	public Collection<ConfirmedMatch> getConfirmedMatches(int ownAPB, int apb, String CNK) throws Exception {
		ResultSet result = null;
        Collection<ConfirmedMatch> confirmedMatches = new ArrayList<ConfirmedMatch>();
        String sql = "SELECT * FROM \"Afstudeerproject\".\"ConfirmedMatches\" WHERE \"APB-match\"=? OR \"ProductAangeboden\"=? OR \"ProductenGevraagd\"=? ORDER BY \"Timestamp\" DESC";
        try {
            String url = getProperties().getProperty("url");
            connection = DriverManager.getConnection(url, getProperties());
 
            statement = connection.prepareStatement(sql);
            statement.setInt(1, apb);
            statement.setString(2, CNK);
            statement.setString(3, CNK);
            result = statement.executeQuery();
            while(result.next()) {
                int match = result.getInt("APB-match");
                int match2 = result.getInt("APB-match2");
                int matchResult = 0;
                if (match == ownAPB)
                	matchResult = match2;
                else
                	matchResult = match;
                String productAangeboden = result.getString("ProductAangeboden");
                int aantalAangeboden = result.getInt("AantalAangeboden");
                String productGevraagd = result.getString("ProductenGevraagd");
                int aantalGevraagd = result.getInt("AantalGevraagd");
                String opmerkingen = result.getString("Opmerkingen");
                Timestamp timestamp = result.getTimestamp("Timestamp");
                
                ConfirmedMatch m = new ConfirmedMatch(productAangeboden, matchResult, aantalAangeboden, productGevraagd, aantalGevraagd, timestamp, opmerkingen);
                confirmedMatches.add(m);
            }
            return confirmedMatches;
        } catch(SQLException e) {
            throw new Exception(e.getMessage());
        } finally {
            closeConnection();
        }
	}


    
}
