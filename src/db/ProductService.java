package db;

import java.util.Collection;
import java.util.Properties;

import domain.Apotheek;
import domain.Product;
import domain.Stock;

public class ProductService {
	private ProductRepositoryDB productRepository;
	
	public ProductService(Properties properties) {
		productRepository = new ProductRepositoryDB(properties);
	}

	public Product getProduct(String cnk) {
		return productRepository.getProduct(cnk);
	}

	public void addProduct(Apotheek a, Product p, int aantal, String typeProduct, String datum) throws Exception {
		productRepository.addProduct(a, p, aantal, typeProduct, datum);
		
	}
	
	public Collection<Stock> getAllTekort(int apb) throws Exception{
		return productRepository.getAllTekort(apb);
	}
	
	public Collection<Stock> getAllOverschot(int apb) throws Exception{
		return productRepository.getAllOverschot(apb);
	}

	public void deleteTeKort(String product, int apb) {
		productRepository.deleteTeKort(product, apb);
		
	}

	public void deleteOverschot(String product, int apb, String vervaldatum) {
		productRepository.deleteOverschot(product, apb, vervaldatum);
		
	}

	public void deleteVervaldatum() {
		productRepository.deleteVervaldatum();
		
	}
	
	public void deleteFromStockAanbieder(int aanbieder, String aanbod, int aangebodenAantal) throws Exception {
		productRepository.deleteFromStockAanbieder(aanbieder, aanbod, aangebodenAantal);
		
	}

	public void deleteFromStockVrager(int vrager, String gevraagd, int gevraagdAantal) throws Exception {
		productRepository.deleteFromStockVrager(vrager, gevraagd, gevraagdAantal);
		
	}
}
