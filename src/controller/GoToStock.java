package controller;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.ProductService;
import domain.Apotheek;
import domain.Stock;

public class GoToStock extends RequestHandler {
	ProductService ps;
		public GoToStock(ProductService ps) {
			this.ps = ps;
		}
		
		@Override
		public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
			Apotheek ap = (Apotheek) request.getSession().getAttribute("apotheek");
			int apb = ap.getApb();
			Collection<Stock> overschotLijst = ps.getAllOverschot(apb);
			Collection<Stock> tekortLijst = ps.getAllTekort(apb);
			request.setAttribute("overschotLijst", overschotLijst);
			request.setAttribute("tekortLijst", tekortLijst);
			return "stock.jsp";
		}
}
