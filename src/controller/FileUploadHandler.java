package controller;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import db.ApotheekService;
import domain.Apotheek;


public class FileUploadHandler extends RequestHandler {
	private final String UPLOAD_DIRECTORY = "C:\\Users\\Jurgen\\git\\afstudeerproject-apothekers\\WebContent\\uploadedImages";
	private ApotheekService as;
	
	public FileUploadHandler (ApotheekService as) {
		this.as = as;
	}
	
    @SuppressWarnings("unchecked")
	public String handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, Exception 
    {
    	String fileName = "";
    	List<String> errors = new ArrayList<String>();
    	Apotheek a = (Apotheek) request.getSession().getAttribute("apotheek");
    	try {
    	if(ServletFileUpload.isMultipartContent(request))
        {
            
                List<FileItem> multiparts = new ServletFileUpload(
                                         new DiskFileItemFactory()).parseRequest(request);
              
                for(FileItem item : multiparts){
                    if(!item.isFormField()){
                    	int startEextension = item.getName().indexOf(".");
                    	String extension = item.getName().substring(startEextension);
                    	if (extension.equals(".png"))
                    		{
                    			String name = new File("profile_" + a.getApb() + extension).getName();
                    			fileName = name;
                    			item.write( new File(UPLOAD_DIRECTORY + File.separator + name));
                    		}
                    	else
                    	{
                    		errors.add("Gelieve een file te uploaden in .png formaat");
                			request.setAttribute("errors", errors);
                			return "Controller?action=profile";
                    	}
                        
                    }
                    
                }
        }
    	else {
        	errors.add("Gelieve een file te uploaden in .png formaat");
        	request.setAttribute("errors", errors);
        	return "Controller?action=profile";
        }
    	} catch (Exception ex) {
    		errors.add("Gelieve een file te uploaden in .png formaat");
			request.setAttribute("errors", errors);
        	return "Controller?action=profile";
        } 
        


        return "Controller?action=profile";
    }
}


