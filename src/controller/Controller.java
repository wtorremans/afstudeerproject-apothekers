package controller;
 
import java.io.IOException;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import db.ApotheekService;
import db.ChatService;
import db.MatchService;
import db.ProductService;
 
 
/**
 * Servlet implementation class Controller
 */
@WebServlet("/Controller")
public class Controller extends HttpServlet {
    private static final long serialVersionUID = 1L;
       ApotheekService apotheekService;
       ProductService productService;
       ChatService chatService;
       MatchService matchService;
       ControllerFactory controllerFactory;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public void init() throws ServletException {
        super.init();
       
        ServletContext context = getServletContext();
 
        Properties properties = new Properties();
        Enumeration<String> parameterNames = context.getInitParameterNames();
        while (parameterNames.hasMoreElements()) {
            String propertyName = parameterNames.nextElement();
            properties.setProperty(propertyName, context.getInitParameter(propertyName));
        }
       
        apotheekService = new ApotheekService(properties);
        productService = new ProductService(properties);
        chatService = new ChatService(properties);
        matchService = new MatchService(properties);
        controllerFactory = new ControllerFactory(apotheekService, productService, chatService, matchService);
    }
   
    public Controller() {
        super();
        // TODO Auto-generated constructor stub
    }
 
    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
 
    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        try {
            processRequest(request, response);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws SQLException, Exception{
        String action = request.getParameter("action");
        if(action == null) {
            action = "GoToHomePage";
        }
       
        RequestHandler requestHandler = ControllerFactory.getController(action);
       
        if (action.equals("GetMessages") || action.equals("sendMessage") || action.equals("GetTekort") || action.equals("GetOverschot")) {
        	String JSON = requestHandler.handleRequest(request, response);
        	response.getWriter().write(JSON);
        } else {
            String destination = requestHandler.handleRequest(request, response);
            if(request.getSession().getAttribute("apotheek") != null) {
                request.setAttribute("apotheek", request.getSession().getAttribute("apotheek"));
            }
            if(destination != null) {
                RequestDispatcher view = request.getRequestDispatcher(destination);
                view.forward(request,  response);
            }
        }
    }
}