package controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.MatchService;
import db.ProductService;
import domain.Apotheek;
import domain.ConfirmedMatch;
import domain.Match;

public class BevestigMatch extends RequestHandler {
	private MatchService ms;
	private ProductService ps;

	public BevestigMatch(MatchService matchService, ProductService ps) {
		this.ps = ps;
		this.ms = matchService;
	}

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, Exception {
		// TODO Auto-generated method stub
		String gevraagd = request.getParameter("aangebodenProduct");
		String aanbod = request.getParameter("gevraagdProduct");
		int aanbieder = Integer.parseInt(request.getParameter("apb"));
		Apotheek a = (Apotheek) request.getSession().getAttribute("apotheek");
		int vrager = a.getApb();
		int gevraagdAantal = 0;
		int aangebodenAantal = 0;
		if (!request.getParameter("aantalGevraagd").equals("") && request.getParameter("aantalAangeboden").equals("")) {
			gevraagdAantal	= Integer.parseInt(request.getParameter("aantalGevraagd"));
			
			ps.deleteFromStockAanbieder(aanbieder, aanbod, gevraagdAantal);
			ps.deleteFromStockVrager(vrager, aanbod, gevraagdAantal);

			String opmerkingen = request.getParameter("extraOpmerkingen");
			Match m = new Match(aanbod, aanbieder, aangebodenAantal, 0, null, vrager, gevraagd, gevraagdAantal, 0, 0);
			ms.addToConfirmedMatches(m, a, opmerkingen);
			
			return "Controller?action=matches";
		}
			
		if (!request.getParameter("aantalAangeboden").equals("") && request.getParameter("aantalGevraagd").equals("")) {
			aangebodenAantal = Integer.parseInt(request.getParameter("aantalAangeboden"));
			vrager = aanbieder;
			aanbieder = a.getApb();
			ps.deleteFromStockAanbieder(aanbieder, gevraagd, aangebodenAantal);
			ps.deleteFromStockVrager(vrager, gevraagd, aangebodenAantal);
			
			String opmerkingen = request.getParameter("extraOpmerkingen");
			Match m = new Match(aanbod, aanbieder, aangebodenAantal, 0, null, vrager, gevraagd, gevraagdAantal, 0, 0);
			ms.addToConfirmedMatches(m, a, opmerkingen);
			return "Controller?action=matches";
		}
		
		gevraagdAantal	= Integer.parseInt(request.getParameter("aantalGevraagd"));
		aangebodenAantal = Integer.parseInt(request.getParameter("aantalAangeboden"));
		ps.deleteFromStockAanbieder(aanbieder, aanbod, gevraagdAantal);
		ps.deleteFromStockVrager(vrager, aanbod, gevraagdAantal);
		ps.deleteFromStockAanbieder(vrager, gevraagd, aangebodenAantal);
		ps.deleteFromStockVrager(aanbieder, gevraagd, aangebodenAantal);

		String opmerkingen = request.getParameter("extraOpmerkingen");
		Match m = new Match(aanbod, aanbieder, aangebodenAantal, 0, null, vrager, gevraagd, gevraagdAantal, 0, 0);
		ms.addToConfirmedMatches(m, a, opmerkingen);
		return "Controller?action=matches";
	}

}
