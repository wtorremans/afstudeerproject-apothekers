package controller;

import java.sql.SQLException;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.ApotheekService;
import domain.Leverancier;

public class GoToRegistrationPage extends RequestHandler {
		private ApotheekService as;
	
		public GoToRegistrationPage(ApotheekService as) {
			this.as = as;
		}
		
		@Override
		public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws SQLException {
			Collection<Leverancier> leveranciers = as.getLeveranciers();
			
			request.setAttribute("leveranciers", leveranciers);
			
			return "register.jsp";
		}
}
