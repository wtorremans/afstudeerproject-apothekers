package controller;

import java.sql.SQLException;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.MatchService;
import domain.Apotheek;
import domain.ConfirmedMatch;

public class GetMatchesAPB extends RequestHandler {
	private MatchService ms;
	public GetMatchesAPB(MatchService matchService) {
		ms = matchService;
	}

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, Exception {
		int apb = Integer.parseInt(request.getParameter("apb"));
		Apotheek a = (Apotheek) request.getSession().getAttribute("apotheek");
		int ownAPB = a.getApb();
		Collection<ConfirmedMatch> matches = ms.getConfirmedMatches(ownAPB, apb, null);
		request.setAttribute("matches", matches);
		return "Controller?action=history";
	}

}
