package controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.MatchService;
import domain.Apotheek;

public class GoToHistory extends RequestHandler {
	private MatchService ms;
		public GoToHistory(MatchService ms) {
			this.ms = ms;
		}
		
		@Override
		public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
			Apotheek a = (Apotheek) request.getSession().getAttribute("apotheek");
			if (request.getAttribute("matches") == null)
				request.setAttribute("matches", ms.getConfirmedMatches(a.getApb()));
			else
				request.getAttribute("matches");
			return "history.jsp";
		}
}
