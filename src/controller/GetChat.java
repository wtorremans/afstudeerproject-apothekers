package controller;

import java.sql.SQLException;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.ChatService;
import domain.Apotheek;
import domain.Chat;
import domain.Message;

public class GetChat extends RequestHandler {
	private ChatService cs;
	
	public GetChat(ChatService cs) {
		this.cs = cs;
	}
	
	
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, Exception {
		Apotheek gebruiker = (Apotheek) request.getSession().getAttribute("apotheek");
		int apbMatch = Integer.parseInt(request.getParameter("apotheker"));
		Apotheek match = cs.getApotheek(apbMatch);
		
		Chat c = cs.getChat(gebruiker, match);
		if (c != null) {
			request.setAttribute("chat", c);
			return "Controller?action=chat";
		}
		
		return "Controller?action=chat";
	}

}
