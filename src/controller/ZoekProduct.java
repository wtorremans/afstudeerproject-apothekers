package controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.ProductService;
import domain.Product;

public class ZoekProduct extends RequestHandler {
	private ProductService ps;
	
	public ZoekProduct(ProductService ps) {
		this.ps = ps;
	}

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, Exception {
		try {
			String cnk = request.getParameter("cnk");
			
			Product p = ps.getProduct(cnk);
			
			request.getSession().setAttribute("product", p);
			GoToStock go = new GoToStock(ps);
			
			return go.handleRequest(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			return "index.jsp";
		}
	}
}
