package controller;

import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.ProductService;
import domain.Apotheek;

public class DeleteOverschot extends RequestHandler {
	private ProductService ps;
	
	public DeleteOverschot(ProductService ps) {
		this.ps = ps;
	}
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, Exception {
		String product = request.getParameter("product");
		String vervaldatum = request.getParameter("vervaldatum");
		Apotheek a = (Apotheek) request.getSession().getAttribute("apotheek");
		int apb = a.getApb();
		ps.deleteOverschot(product, apb, vervaldatum);

		return "Controller?action=stock";
	}

}
