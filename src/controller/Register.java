package controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.ApotheekService;
import domain.Apotheek;

public class Register extends RequestHandler{
	private ApotheekService as;
    private final String UPLOAD_DIRECTORY = "images/";
	
	public Register(ApotheekService as) {
		this.as = as;
	}
	
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws SQLException {
		List<String> errors = new ArrayList<String>();
		Apotheek a = null;
		
		String apbString = request.getParameter("apb");
		String paswoord = request.getParameter("paswoord");
		String repeatPaswoord = request.getParameter("repeatPaswoord");
		String email = request.getParameter("email");
		String telefoon = request.getParameter("telefoon");
		String gsm = request.getParameter("gsm");
		String straat = request.getParameter("straat");
		String gemeente = request.getParameter("gemeente");
		String naam = request.getParameter("naam");
		String nummerString = request.getParameter("nummer");
		String postcodeString = request.getParameter("postcode");
		String fileName = "profile_default.png";
		String leverancier = request.getParameter("leverancier");
          
        
		
		if (!apbString.matches("-?\\d+(\\.\\d+)?")) {
			errors.add("Gelieve een getal in te vullen voor APB-nummer");
			request.setAttribute("errors", errors);
			return "Controller?action=registrationPage";
		}
		if (!postcodeString.matches("-?\\d+(\\.\\d+)?")) {
			errors.add("Gelieve een getal in te vullen voor postcode");
			request.setAttribute("errors", errors);
			return "Controller?action=registrationPage";
		}
		if (apbString.equals("") || paswoord.equals("") || repeatPaswoord.equals("") || email.equals("") || telefoon.equals("") || gsm.equals("") || straat.equals("") || gemeente.equals("") || naam.equals("") || nummerString.equals("") || postcodeString.equals("")) {
			errors.add("Gelieve alle vakken in te vullen");
			request.setAttribute("errors", errors);
			return "Controller?action=registrationPage";
		} else if (!paswoord.equals(repeatPaswoord)) {
			errors.add("Twee paswoorden komen niet overeen");
			request.setAttribute("errors", errors);
			return "Controller?action=registrationPage";
		} else if (as.getApotheek(Integer.parseInt(apbString)) != null) {
			errors.add("U heeft al een account, gelieve in te loggen");
			request.setAttribute("errors", errors);
			return "index.jsp";
		} else {
			int apb = Integer.parseInt(apbString);
			int nummer = Integer.parseInt(nummerString);
			int postcode = Integer.parseInt(postcodeString);
			a = new Apotheek(apb, paswoord, email, telefoon, gsm, straat, gemeente, naam, nummer, postcode, fileName, leverancier);
			as.add(a);
			return "index.jsp";
		}

	}

}
