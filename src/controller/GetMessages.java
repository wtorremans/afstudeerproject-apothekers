package controller;

import java.sql.SQLException;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import db.ChatService;
import domain.Apotheek;
import domain.Chat;
import domain.Message;

public class GetMessages extends RequestHandler {
	private ChatService cs;
	
	public GetMessages(ChatService cs) {
		this.cs = cs;
	}
	
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, Exception {
		Apotheek gebruiker = (Apotheek) request.getSession().getAttribute("apotheek");
		int apbMatch = Integer.parseInt(request.getParameter("friend"));
		Apotheek match = cs.getApotheek(apbMatch);
		
		Chat c = cs.getChat(gebruiker, match);
		Collection<Message> messages = cs.getMessages(c.getId());
		
		Gson gson = new Gson();
		String messagesJSON = "{\"messages\":" + gson.toJson(messages) + "}";
		request.setAttribute("messagesOverview", messagesJSON);
		return messagesJSON;
	}

}
