package controller;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.ApotheekService;
import domain.Apotheek;

public class UpdatePassword extends RequestHandler{
	private ApotheekService as;
	
	public UpdatePassword(ApotheekService as) {
		this.as = as;
	}
	
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		try{
			Apotheek ap = (Apotheek) request.getSession().getAttribute("apotheek");
			int apb = ap.getApb();

			String nieuwPaswoord = hashPaswoord(request.getParameter("nieuwPaswoord"));
			String herhaalPaswoord = hashPaswoord(request.getParameter("herhaalPaswoord"));
			if (!nieuwPaswoord.equals(herhaalPaswoord)) {
				throw new IllegalArgumentException("fout");
			}
			Apotheek a = as.updatePassword(apb, nieuwPaswoord);
			request.getSession().setAttribute("apotheek", a);
			
			return "Controller?action=profile";
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "index.jsp";
		}

	}
	
	private String hashPaswoord(String paswoord) {
		String encrypted = paswoord;
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-1");
			digest.reset();
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalArgumentException(e.getMessage(), e);
		}
		digest.update(paswoord.getBytes());
		byte[] encryptedBytes = new byte[40];
		encryptedBytes=digest.digest();
		encrypted = new BigInteger(1, encryptedBytes).toString(16);
		return encrypted;
	}
}
