package controller;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.ApotheekService;
import db.ProductService;
import domain.Apotheek;

public class Login extends RequestHandler{
	private ApotheekService as;
	private ProductService ps;
	
	public Login(ApotheekService as, ProductService ps) {
		this.as = as;
		this.ps = ps;
	}
	
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws SQLException {
		List<String> errors = new ArrayList<String>();
		Apotheek a = null;
		
		String apbString = request.getParameter("apb");
		String paswoord = hashPaswoord(request.getParameter("paswoord"));
		if (!apbString.matches("-?\\d+(\\.\\d+)?")) {
			errors.add("Gelieve een getal in te vullen voor APB-nummer");
			request.setAttribute("errors", errors);
			return "index.jsp";
		}
		if (paswoord.equals("") || apbString.equals("")) {
			errors.add("Gelieve beide in te vullen");
			request.setAttribute("errors", errors);
			return "index.jsp";
		} else {
			int apb = Integer.parseInt(apbString);
			a = as.getApotheekIfAuthenticated(apb, paswoord);
			
		}
		if(a == null) {
			errors.add("Inloggen mislukt, verkeerd APB-nummer of paswoord");
			request.setAttribute("errors", errors);
		} else {
			request.getSession().setAttribute("apotheek", a);
			ps.deleteVervaldatum();
		}
		
		
		return "index.jsp";
	}
	
	private String hashPaswoord(String paswoord) {
		String encrypted = paswoord;
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-1");
			digest.reset();
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalArgumentException(e.getMessage(), e);
		}
		digest.update(paswoord.getBytes());
		byte[] encryptedBytes = new byte[40];
		encryptedBytes=digest.digest();
		encrypted = new BigInteger(1, encryptedBytes).toString(16);
		return encrypted;
	}
	
}
