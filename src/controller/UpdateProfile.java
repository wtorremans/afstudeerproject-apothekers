package controller;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.ApotheekService;
import domain.Apotheek;

public class UpdateProfile extends RequestHandler{
	private ApotheekService as;
    private final String UPLOAD_DIRECTORY = "images/";
	
	public UpdateProfile(ApotheekService as) {
		this.as = as;
	}
	
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Apotheek a = null;
		try{
			int apb = Integer.parseInt(request.getParameter("apb"));
			
			String paswoord = ((Apotheek) request.getSession().getAttribute("apotheek")).getPaswoord();
			String email = request.getParameter("email");
			String telefoon = request.getParameter("telefoon");
			String gsm = request.getParameter("gsm");
			String straat = request.getParameter("straat");
			String gemeente = request.getParameter("gemeente");
			String naam = request.getParameter("naam");
			int nummer = Integer.parseInt(request.getParameter("nummer"));
			int postcode = Integer.parseInt(request.getParameter("postcode"));
			String fileName = ((Apotheek) request.getSession().getAttribute("apotheek")).getFileName();
			String leverancier = request.getParameter("leverancier");
			a = as.update(apb, paswoord, email, telefoon, gsm, straat, gemeente, naam, nummer, postcode, fileName, leverancier);
			
			request.getSession().setAttribute("apotheek", a);

			return "Controller?action=profile";
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "index.jsp";
		}

	}
}
