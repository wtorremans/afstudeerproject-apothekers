package controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import db.ApotheekService;
import db.ChatService;
import db.MatchService;
import db.ProductService;

public class ControllerFactory {
		private static Map<String, RequestHandler> handlers = new HashMap<>();
		
		
		public ControllerFactory(ApotheekService apotheekService, ProductService productService, ChatService chatService, MatchService matchService) throws ServletException {
			handlers.put("home", new GoToHome());
			handlers.put("profile", new GoToProfile(apotheekService));
			handlers.put("logout", new Logout());
			
			handlers.put("login", new Login(apotheekService, productService));
			handlers.put("updateProfile", new UpdateProfile(apotheekService));
			handlers.put("register", new Register(apotheekService));
			handlers.put("registrationPage", new GoToRegistrationPage(apotheekService));
			handlers.put("updatePassword", new UpdatePassword(apotheekService));
			handlers.put("uploadPicture", new FileUploadHandler(apotheekService));
			
			handlers.put("zoekProduct", new ZoekProduct(productService));
			handlers.put("voegProductToe", new VoegProductToe(productService));
			handlers.put("deleteTeKort", new DeleteTeKort(productService));
			handlers.put("deleteOverschot", new DeleteOverschot(productService));
			handlers.put("stock", new GoToStock(productService));
			
			handlers.put("matches", new GoToMatches(matchService));
			handlers.put("bevestigMatch", new BevestigMatch(matchService, productService));
			handlers.put("GetMatchesAPB", new GetMatchesAPB(matchService));
			handlers.put("GetMatchesCNK", new GetMatchesCNK(matchService));
			handlers.put("history", new GoToHistory(matchService));
			
			handlers.put("createChat", new CreateChat(chatService));
			handlers.put("deleteChat", new DeleteChat(chatService));
			handlers.put("getChat", new GetChat(chatService));
			handlers.put("sendMessage", new SendMessage(chatService));
			handlers.put("chat", new GoToChat(chatService));
			handlers.put("GetMessages", new GetMessages(chatService));
			
		}
		
		public static RequestHandler getController(String key) {
			return handlers.get(key);
		}
}
