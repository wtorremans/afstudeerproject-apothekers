package controller;

import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.ProductService;
import domain.Apotheek;

public class DeleteTeKort extends RequestHandler {
	private ProductService ps;
	
	public DeleteTeKort(ProductService ps) {
		this.ps = ps;
	}
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, Exception {
		String product = request.getParameter("product");
		Apotheek a = (Apotheek) request.getSession().getAttribute("apotheek");
		int apb = a.getApb();
		ps.deleteTeKort(product, apb);
		
		return "Controller?action=stock";
	}

}
