package controller;

import java.sql.SQLException;
import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.ChatService;
import domain.Apotheek;
import domain.Chat;
import domain.Message;

public class SendMessage extends RequestHandler {
	private ChatService cs;
	
	public SendMessage(ChatService cs) {
		this.cs = cs;
	}
	
	
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, Exception {
		Apotheek sender = (Apotheek) request.getSession().getAttribute("apotheek");
		int senderApb = sender.getApb();
		int chat = Integer.parseInt(request.getParameter("chatId"));
		int receiverapb = Integer.parseInt(request.getParameter("receiver"));
		Apotheek receiver = cs.getApotheek(receiverapb);
		String text = request.getParameter("message");
		if (text == null) {
			return "Controller?action=getMessages&friend=" + receiverapb;
		}
		Message m = new Message(text, sender, receiver, new Timestamp(System.currentTimeMillis()));
		
		cs.sendMessage(text, senderApb, receiverapb, chat);
		Chat c = cs.getChat(sender, receiver);
		c.addMessage(m);
		
		return "Controller?action=getMessages&friend=" + receiverapb;
	}

}
