package controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.ChatService;
import domain.Apotheek;
import domain.Chat;

public class CreateChat extends RequestHandler {
	private ChatService cs;
	
	public CreateChat(ChatService cs) {
		this.cs = cs;
	}
	
	
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, Exception {
		Apotheek a1 = (Apotheek) request.getSession().getAttribute("apotheek");
		int apb1 = a1.getApb();
		int apb2 = Integer.parseInt(request.getParameter("apotheker"));
		Apotheek a2 = cs.getApotheek(apb2);
		String message = request.getParameter("message");
		
		int id = cs.getMaxChats() + 1;
		cs.createChat(a1, a2, id);
		Chat c = cs.getChat(a1, cs.getApotheek(apb2));
		cs.sendMessage(message, apb1, apb2, c.getId());
		
		return "Controller?action=chat";
	}

}
