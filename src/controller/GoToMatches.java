package controller;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import db.MatchService;
import domain.Apotheek;
import domain.Match;

public class GoToMatches extends RequestHandler {
		private MatchService ms;
	
		public GoToMatches(MatchService ms) {
			this.ms = ms;
		}
		
		@Override
		public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
			Apotheek a = (Apotheek) request.getSession().getAttribute("apotheek");
			int apb = a.getApb();
			
			Collection<Match> fullMatches = ms.getFullMatch(apb);
			Collection<Match> vraagMatches = ms.getVraagMatch(apb);
			Collection<Match> aanbodMatches = ms.getAanbodMatch(apb);
			
			request.setAttribute("FullMatchLijst", fullMatches);
			request.setAttribute("VraagMatchLijst", vraagMatches);
			request.setAttribute("AanbodMatchLijst", aanbodMatches);
			
			return "matches.jsp";
		}
}
