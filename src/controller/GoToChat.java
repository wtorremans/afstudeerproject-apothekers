package controller;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.ChatService;
import domain.Apotheek;
import domain.Chat;

public class GoToChat extends RequestHandler {
	private ChatService cs;
	
	public GoToChat(ChatService cs) {
		this.cs = cs;
	}
		
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Apotheek a1 = (Apotheek) request.getSession().getAttribute("apotheek");
		int apb1 = a1.getApb();
		
		Collection<Chat> chats = cs.getAllChatsFor(apb1);
		request.setAttribute("chats", chats);
		if (request.getAttribute("chat") != null) {
			return "chat.jsp";
		}
		request.setAttribute("chat", null);
		
		return "chat.jsp";
	}
}
