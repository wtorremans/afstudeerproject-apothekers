package controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GoToHome extends RequestHandler {
		public GoToHome() {
		}
		
		@Override
		public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws SQLException {
			return "index.jsp";
		}
}
