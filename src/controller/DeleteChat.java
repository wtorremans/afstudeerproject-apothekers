package controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.ChatService;

public class DeleteChat extends RequestHandler {
	private ChatService cs;
	
	public DeleteChat(ChatService cs) {
		this.cs = cs;
	}
	
	
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, Exception {
		int id = Integer.parseInt(request.getParameter("id"));
		
		cs.deleteChat(id);
		
		return "Controller?action=chat";
	}

}
