package domain;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Apotheek {
	private int apb;
	private String paswoord;
	private String email;
	private String naam;
	private String straat;
	private String telefoon;
	private String gsm;
	private String gemeente;
	private int nummer;
	private int postcode;
	private String fileName;
	private String leverancier;
	
	public Apotheek(int apb, String paswoord, String email, String telefoon, String gsm, String straat, String gemeente, String naam, int nummer, int postcode, String fileName, String leverancier) {
		setApb(apb);
		setPaswoord(paswoord);
		setEmail(email);
		setTelefoon(telefoon);
		setGsm(gsm);
		setGemeente(gemeente);
		setStraat(straat);
		setNaam(naam);
		setNummer(nummer);
		setPostcode(postcode);
		setFileName(fileName);
		setLeverancier(leverancier);
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
		
	}

	public void setApb(int apb) {
		this.apb = apb;
	}
	
	public void setPaswoord(String paswoord) {
		setHashedPassword(paswoord);
	}
	
	public void setHashedPassword(String paswoord) {
		if (paswoord == null || paswoord.isEmpty()) {
			throw new IllegalArgumentException("Paswoord mag niet leeg zijn");
		}
		this.paswoord = hashPaswoord(paswoord);
	}
	
	public boolean isPasswordCorrect(String paswoord) {
		String hashedPassword = hashPaswoord(paswoord);
		return getPaswoord().equals(hashedPassword);
	}
	
	private String hashPaswoord(String paswoord) {
		String encrypted = paswoord;
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-1");
			digest.reset();
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalArgumentException(e.getMessage(), e);
		}
		digest.update(paswoord.getBytes());
		byte[] encryptedBytes = new byte[40];
		encryptedBytes=digest.digest();
		encrypted = new BigInteger(1, encryptedBytes).toString(16);
		return encrypted;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public int getApb() {
		return apb;
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public String getPaswoord() {
		return paswoord;
	}
	
	public String getEmail() {
		return email;
	}

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public String getStraat() {
		return straat;
	}

	public void setStraat(String straat) {
		this.straat = straat;
	}

	public String getTelefoon() {
		return telefoon;
	}

	public void setTelefoon(String telefoon) {
		this.telefoon = telefoon;
	}

	public String getGsm() {
		return gsm;
	}

	public void setGsm(String gsm) {
		this.gsm = gsm;
	}

	public String getGemeente() {
		return gemeente;
	}

	public void setGemeente(String gemeente) {
		this.gemeente = gemeente;
	}

	public int getNummer() {
		return nummer;
	}

	public void setNummer(int nummer) {
		this.nummer = nummer;
	}

	public int getPostcode() {
		return postcode;
	}

	public void setPostcode(int postcode) {
		this.postcode = postcode;
	}

	public String getLeverancier() {
		return leverancier;
	}

	public void setLeverancier(String leverancier) {
		this.leverancier = leverancier;
	}
}
