package domain;

import java.sql.Timestamp;

public class Message {
	private String message;
	private Apotheek sender;
	private Apotheek receiver;
	private Timestamp time;
	
	public Message(String message, Apotheek sender, Apotheek receiver, Timestamp time) {
		setMessage(message);
		setSender(sender);
		setReceiver(receiver);
		setTime(time);
		
		
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Apotheek getSender() {
		return sender;
	}

	public void setSender(Apotheek sender) {
		this.sender = sender;
	}

	public Apotheek getReceiver() {
		return receiver;
	}

	public void setReceiver(Apotheek receiver) {
		this.receiver = receiver;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}
}
