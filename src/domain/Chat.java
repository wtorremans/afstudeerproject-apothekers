package domain;

public class Chat {
	private int id;
	private Apotheek a1;
	private Apotheek a2;
	private int unreadMessagesA1 = 0;
	private int unreadMessagesA2 = 0;
	
	public Chat (int id, Apotheek a1, Apotheek a2) {
		setId(id);
		setA1(a1);
		setA2(a2);
	}

	public Apotheek getA1() {
		return a1;
	}

	public void setA1(Apotheek a1) {
		this.a1 = a1;
	}

	public Apotheek getA2() {
		return a2;
	}

	public void setA2(Apotheek a2) {
		this.a2 = a2;
	}
	
	public int getUnreadMessagesA1() {
		return unreadMessagesA1;
	}
	
	public int getUnreadMessagesA2() {
		return unreadMessagesA2;
	}
	
	public void readAllMessages(Apotheek a){
		if(a.equals(a1)){
			unreadMessagesA1 = 0;
		} else if(a.equals(a2)){
			unreadMessagesA2 = 0;
		} else {
			throw new IllegalArgumentException("This user is not in the chat.");
		}
	}
	
	public void addMessage(Message m) {
		if (m.getReceiver().getApb() == this.a1.getApb()) {
			unreadMessagesA1++;
		} else if(m.getReceiver().getApb() == this.a2.getApb()) {
			unreadMessagesA2++;
		} else {
			throw new IllegalArgumentException("This user is not in the chat");
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
