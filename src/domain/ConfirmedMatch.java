package domain;

import java.sql.Timestamp;

public class ConfirmedMatch {
    private int match;
    private int aangebodenAantal;
    private String productGevraagd;
    private String productAangeboden;
    private int gevraagdAantal;
    private Timestamp timestamp;
    private String opmerkingen;
   
   
    public ConfirmedMatch(String productAangeboden, int match, int aangebodenAantal, String productGevraagd, int gevraagdAantal, Timestamp timestamp, String opmerkingen) {
        setProductAangeboden(productAangeboden);
        setMatch(match);
        setAangebodenAantal(aangebodenAantal);
        setProductGevraagd(productGevraagd);
        setGevraagdAantal(gevraagdAantal);
        setTimestamp(timestamp);
        setOpmerkingen(opmerkingen);
    }
   
   
    public String getProductAangeboden() {
        return productAangeboden;
    }
    public void setProductAangeboden(String productAangeboden) {
        this.productAangeboden = productAangeboden;
    }
    public int getMatch() {
        return match;
    }
    public void setMatch(int match) {
        this.match = match;
    }
    public int getAangebodenAantal() {
        return aangebodenAantal;
    }
    public void setAangebodenAantal(int aangebodenAantal) {
        this.aangebodenAantal = aangebodenAantal;
    }  
    public String getProductGevraagd() {
        return productGevraagd;
    }
    public void setProductGevraagd(String productGevraagd) {
        this.productGevraagd = productGevraagd;
    }
    public int getGevraagdAantal() {
        return gevraagdAantal;
    }
    public void setGevraagdAantal(int gevraagdAantal) {
        this.gevraagdAantal = gevraagdAantal;
    }
    
    public Timestamp getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}


	public String getOpmerkingen() {
		return opmerkingen;
	}


	public void setOpmerkingen(String opmerkingen) {
		this.opmerkingen = opmerkingen;
	}


   
   
}