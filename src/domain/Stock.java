package domain;

public class Stock {

	private double prijs;
	private int aantal;
	private int apotheek;
	private String cnk;
	private String vervalDatum;

	public Stock(int a, String cnk, int aantal, double prijs, String vervalDatum) {
		this.setApotheek(a);
		this.setCNK(cnk);
		this.setAantal(aantal);
		this.setPrijs(prijs);
		this.setVervalDatum(vervalDatum);
	}

	private void setPrijs(double prijs) {
		this.prijs = prijs;
		
	}

	public double getPrijs() {
		return prijs;
	}

	public int getAantal() {
		return aantal;
	}

	public int getApotheek() {
		return apotheek;
	}

	public String getCnk() {
		return cnk;
	}

	private void setAantal(int aantal) {
		this.aantal = aantal;
		
	}

	private void setCNK(String cnk) {
		this.cnk = cnk;
		
	}

	private void setApotheek(int a) {
		this.apotheek = a;
		
	}

	public String getVervalDatum() {
		return vervalDatum;
	}

	public void setVervalDatum(String vervalDatum) {
		this.vervalDatum = vervalDatum;
	}

}
