package domain;

public class Leverancier {
	private int id;
	private String naam;
	private String straat;
	private int nummer;
	private int postcode;
	private String gemeente;
	
	public Leverancier(int id, String naam, String straat, int nummer, int postcode, String gemeente) {
		setId(id);
		setNaam(naam);
		setNummer(nummer);
		setPostcode(postcode);
		setGemeente(gemeente);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public String getStraat() {
		return straat;
	}

	public void setStraat(String straat) {
		this.straat = straat;
	}

	public int getNummer() {
		return nummer;
	}

	public void setNummer(int nummer) {
		this.nummer = nummer;
	}

	public int getPostcode() {
		return postcode;
	}

	public void setPostcode(int postcode) {
		this.postcode = postcode;
	}

	public String getGemeente() {
		return gemeente;
	}

	public void setGemeente(String gemeente) {
		this.gemeente = gemeente;
	}
}
