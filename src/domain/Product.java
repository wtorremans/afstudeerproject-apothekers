package domain;

public class Product {
	private String code;
	private String description;
	private String serviceCategory;
	private double price;
	
	public Product(String code, String description, String serviceCategory, double price) {
		setCode(code);
		setDescription(description);
		setServiceCategory(serviceCategory);
		setPrice(price);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getServiceCategory() {
		return serviceCategory;
	}

	public void setServiceCategory(String serviceCategory) {
		this.serviceCategory = serviceCategory;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}
