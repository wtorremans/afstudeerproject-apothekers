package domain;
 
public class Match {
    private String productAangeboden;
    private int aanbieder;
    private int aangebodenAantal;
    private int vrager;
    private String productGevraagd;
    private int gevraagdAantal;
    private double prijsVerschil;
    private String vervaldatum;
    private int aantalNodig;
    private int aantalAanbieding;
   
   
    public Match(String productAangeboden, int aanbieder, int aangebodenAantal, int aantalNodig, String vervaldatum, int vrager,
            String productGevraagd, int gevraagdAantal, int aantalAanbieding, double prijsVerschil) {
        setProductAangeboden(productAangeboden);
        setAanbieder(aanbieder);
        setAangebodenAantal(aangebodenAantal);
        setVrager(vrager);
        setProductGevraagd(productGevraagd);
        setGevraagdAantal(gevraagdAantal);
        setPrijsVerschil(prijsVerschil);
        setVervaldatum(vervaldatum);
        setAantalNodig(aantalNodig);
        setAantalAanbieding(aantalAanbieding);
    }
   
   
    public String getProductAangeboden() {
        return productAangeboden;
    }
    public void setProductAangeboden(String productAangeboden) {
        this.productAangeboden = productAangeboden;
    }
    public int getAanbieder() {
        return aanbieder;
    }
    public void setAanbieder(int aanbieder) {
        this.aanbieder = aanbieder;
    }
    public int getAangebodenAantal() {
        return aangebodenAantal;
    }
    public void setAangebodenAantal(int aangebodenAantal) {
        this.aangebodenAantal = aangebodenAantal;
    }
   
    public int getVrager() {
        return vrager;
    }
    public void setVrager(int vrager) {
        this.vrager = vrager;
    }
    public String getProductGevraagd() {
        return productGevraagd;
    }
    public void setProductGevraagd(String productGevraagd) {
        this.productGevraagd = productGevraagd;
    }
    public int getGevraagdAantal() {
        return gevraagdAantal;
    }
    public void setGevraagdAantal(int gevraagdAantal) {
        this.gevraagdAantal = gevraagdAantal;
    }
   
    public double getPrijsVerschil() {
        return prijsVerschil;
    }
    public void setPrijsVerschil(double prijsVerschil) {
        this.prijsVerschil = prijsVerschil;
    }
 
 
    public String getVervaldatum() {
        return vervaldatum;
    }
 
 
    public void setVervaldatum(String vervaldatum) {
        this.vervaldatum = vervaldatum;
    }
 
 
    public int getAantalNodig() {
        return aantalNodig;
    }
 
 
    public void setAantalNodig(int aantalNodig) {
        this.aantalNodig = aantalNodig;
    }
 
 
    public int getAantalAanbieding() {
        return aantalAanbieding;
    }
 
 
    public void setAantalAanbieding(int aantalAanbieding) {
        this.aantalAanbieding = aantalAanbieding;
    }
   
   
}